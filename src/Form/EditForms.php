<?php

namespace Drupal\clickandpledge_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Uses to edit CONNECT forms.
 */
class EditForms extends FormBase {
  const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
  const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * To get the path.
   *
   * @var mixed
   */
  protected $path;

  /**
   * Declaare SOAP version.
   */
  const CFCNP_SOAP_VER = "SOAP_1_1";

  /**
   * CFCNP_SERVICE_URL.
   *
   * Service URL to authenticate and to get forms and campaigns from click
   * and pledge CONNECT platform.
   */
  const CFCNP_SERVICE_URL = "https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl";

  /**
   * The Class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Path\CurrentPathStack $pathStack
   *   Represents the current path for the current request.
   */
  public function __construct(Connection $connection, CurrentPathStack $pathStack) {
    $this->connection = $connection;
    $this->path = $pathStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "cnpc_edit_form_group";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form["#theme"] = 'cnpc_edit_form_group';
    $form = $this->displayEditCnpcForm($form, $form_state);
    return $form;
  }

  /**
   * Creates a form to edit the CONNECT forms.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displayEditCnpcForm(array $form, $form_state) {
    date_default_timezone_set(date_default_timezone_get());
    $path = $this->path->getPath();
    $arg = explode('/', $path);
    $reqID = end($arg);
    $formInfo = $this->getFormInfo($reqID);
    $formDetails = $this->getFormDetails($reqID);

    $form['cnpc_form_group_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Group Name*'),
      '#description' => $this->t('Please enter the form group name<br><span class="cnpc_formexists_error"></span>'),
      '#default_value' => $formInfo->cnpform_groupname,
      '#attributes' => ["id" => "cnpc_form_group_name"],
      '#disabled' => TRUE,
    ];
    $form['hidden_fg_name'] = [
      '#type' => 'hidden',
      '#default_value' => $formInfo->cnpform_groupname,
      '#attributes' => ["id" => "hidden_fg_name"],
    ];
    $data = $this->getRecords();
    $options = [];
    foreach ($data as $rec) {
      $options[$rec->cnpstngs_AccountNumber . "||" . $rec->cnpstngs_guid . "||" . $rec->cnpstngs_ID] = $rec->cnpstngs_frndlyname . ' ( ' . trim($rec->cnpstngs_AccountNumber) . ' )';
    }
    $form['cnpc_form_save_url'] = [
      '#type' => 'hidden',
      '#default_value' => Url::fromRoute('cnpconnect.cnpc_save_form_data', [])->toString(),
      '#attributes' => ['id' => 'cnpc_form_save_url'],
    ];
    $form['base_url_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ["id" => "base_url_cnpc"],
    ];
    $form['cnpc_edit_id'] = [
      '#type' => 'hidden',
      '#default_value' => $reqID,
      '#attributes' => ["id" => "cnpc_edit_id"],
    ];
    // Popup textarea for url params.
    $form['cnpc_params_textarea'] = [
      "#type" => "textarea",
      "#atrributes" => ['class' => ['paramsfeild']],
    ];
    $form['cnpc_params_textarea']['#theme_wrappers'] = [];

    $form['cnpc_params_button_add'] = [
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#attributes' => ['class' => ['popupsavebtn'], "id" => "addparams"],
    ];
    $form['cnpc_params_button_close'] = [
      '#type' => 'button',
      '#value' => $this->t('Close'),
      '#attributes' => ['class' => ['popupclosebtn'], "id" => "closeparams"],
    ];
    $form['cnpc_params_button_saveclose'] = [

      '#type' => 'button',
      '#value' => $this->t('Add & Close'),
      '#attributes' => ['class' => ['popupsaveclosebtn'], "id" => "savecloseparams"],
    ];
    $form['cnpc_action'] = [
      '#type' => 'hidden',
      '#default_value' => 'edit',
      '#attributes' => ["id" => "cnpc_action"],
    ];
    $form['cnpc_accounts_hidden'] = [
      '#type' => 'hidden',
      '#default_value' => $formInfo->cnpform_AccountNumber,
      '#attributes' => ["id" => "cnpc_accounts_hidden"],
    ];
    $form['cnpc_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Account(s)*'),
      '#options' => $options,
      '#default_value' => "",
      '#attributes' => ["id" => "cnpc_accounts"],
      '#disabled' => TRUE,
    ];
    $form['cnpc_start_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Date/Time* @dateC', ['@dateC' => '[TimeZone: ' . date_default_timezone_get() . ']']),
      '#attributes' => ["id" => "start_date_time"],
      '#default_value' => date("F d, Y h:i a", strtotime($formInfo->cnpform_Form_StartDate)),
    ];
    if ($formInfo->cnpform_Form_EndDate == "0000-00-00 00:00:00") {
      $ed = "";
    }
    else {
      $ed = date("F d, Y h:i a", strtotime($formInfo->cnpform_Form_EndDate));
    }
    $form['cnpc_end_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End Date/Time'),
      '#attributes' => ["id" => "end_date_time"],
      '#default_value' => $ed,
    ];
    $form['cnpc_display_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Type'),
      '#options' => ["inline" => $this->t("Inline"), "popup" => $this->t("Overlay")],
      '#attributes' => ["id" => "cnpc_display_type"],
      '#default_value' => $formInfo->cnpform_type,
    ];
    // Hidden form overlay.
    $form['cnpc_link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Link Type*'),
      '#options' => [
        "button" => $this->t("Button"),
        "image" => $this->t("Image"),
        "text" => $this->t("Text"),
      ],
      '#attributes' => ["id" => "cnpc_link_type"],
      '#default_value' => $formInfo->cnpform_ptype,
    ];
    $form['cnpc_buton_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Label*'),
      '#attributes' => ["id" => "cnpc_btn_lbl"],
      '#default_value' => $formInfo->cnpform_text,
    ];
   $form['cnpc_text_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text Label*'),
      '#attributes' => ["id" => "cnpc_txt_lbl"],
      '#default_value' => $formInfo->cnpform_text,
    ];
    $form['cnpc_upload_image'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload Image*'),
      '#attributes' => ["id" => "cnpc_upload_image"],
      '#default_value' => "",
    ];
    $form['cnpc_upload_image_hidden'] = [
      '#type' => 'hidden',
      '#attributes' => ["id" => "cnpc_upload_image_hidden"],
      '#default_value' => $formInfo->cnpform_img,
    ];
    $form['cnpc_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Valid Form Message'),
      '#attributes' => ["id" => "cnpc_message"],
      '#default_value' => $formInfo->cnpform_custommsg,
    ];
    $form['cnpc_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => ["active" => $this->t("Active"), "inactive" => $this->t("Inactive")],
      '#attributes' => ["id" => "cnpc_status"],
      '#default_value' => $formInfo->cnpform_status,
    ];
    // Hidden form Starts here.
    $noofFoms = count($formDetails);
    $form['cnpc_counter'] = [
      '#type' => 'hidden',
      '#attributes' => ["id" => "cnpc_counter"],
      '#default_value' => $noofFoms,
    ];
    $form['save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ["id" => "finalsave"],
    ];
    $form['final_save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ["id" => "updateforms"],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Form validations are hasling with javascript.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Saving form data handling with ajax call.
  }

  /**
   * Get all the accounts to display into the accounts drop down in the form.
   *
   * @return array
   *   Returns an array which contains account details to display in Acoounts
   *   drop down in the form.
   */
  public function getRecords() {
    $sql = "SELECT cnpstngs_ID,cnpstngs_frndlyname,cnpstngs_AccountNumber,cnpstngs_guid,cnpstngs_status,"
            . "cnpstngs_Date_Created,cnpstngs_Date_Modified FROM {dp_cnpcsettingsdtl}";
    $query = $this->connection->query($sql);
    return $query->fetchAll();
  }

  /**
   * To get the forms information under a form form group.
   *
   * @param int $id
   *   Form group id.
   *
   * @return array
   *   Returns the information about a form under a campaign.
   */
  public function getFormInfo($id) {
    $sql = "SELECT cnpform_GID,cnpform_groupname,cnpform_cnpstngs_ID,cnpform_AccountNumber,cnpform_guid,"
            . "cnpform_type,cnpform_ptype,cnpform_text,cnpform_img,cnpform_shortcode,cnpform_custommsg,"
            . "cnpform_Form_StartDate,cnpform_Form_EndDate,cnpform_status,cnpform_Date_Created,"
            . "cnpform_Date_Modified FROM {dp_cnpc_forminfo} where cnpform_GID=:gid";
    $query = $this->connection->query($sql, [':gid' => $id]);
    return $query->fetch();
  }

  /**
   * Get form details to display the forms.
   *
   * @param int $id
   *   Form id.
   *
   * @return array
   *   Returns an array contains individual form details.
   */
  public function getFormDetails($id) {
    $sql = "SELECT cnpform_id,cnpform_cnpform_ID,cnpform_CampaignName,cnpform_FormName,cnpform_GUID,"
            . "cnpform_FormStartDate,cnpform_FormEndDate,cnpform_urlparameters,cnpform_FormStatus,"
            . "cnpform_DateCreated,cnpform_DateModified FROM {dp_cnpc_formsdtl} where cnpform_cnpform_ID=:fid";
    $query = $this->connection->query($sql, [':fid' => $id]);
    return $query->fetchAll();
  }

}
