<?php

namespace Drupal\clickandpledge_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Uses to edit the pledgeTV channels.
 */
class EditChannels extends FormBase {

  /**
   * The Class Constants.
   */
  const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
  const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";
  const CFCNP_SOAP_VER = "SOAP_1_1";

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The module handler service.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * To get the path.
   *
   * @var mixed
   */
  protected $path;

  /**
   * The Class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Path\CurrentPathStack $pathStack
   *   Represents the current path for the current request.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler, CurrentPathStack $pathStack) {
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
    $this->path = $pathStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('module_handler'),
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnpc_edit_channel_group';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form["#theme"] = 'cnpc_edit_channel_group';
    $form = $this->displayEditCnpcForm($form, $form_state);
    return $form;
  }

  /**
   * Create a channel edit form for editing.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displayEditCnpcForm(array $form, $form_state) {
    date_default_timezone_set(date_default_timezone_get());
    $arg = explode('/', $this->path->getPath());
    $reqID = end($arg);
    $formInfo = $this->getChannelInfo($reqID);
    $formDetails = $this->getChannelDetails($reqID);

    $form['cnpc_channel_group_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel Group Name*'),
      '#description' => $this->t('Please enter the channel name<br><span class="cnpc_channelexists_error"></span>'),
      '#default_value' => $formInfo->cnpchannelgrp_groupname,
      '#attributes' => ['id' => 'cnpc_channel_group_name'],
      '#disabled' => TRUE,
    ];
    $form['hidden_fg_name'] = [
      '#type' => 'hidden',
      '#default_value' => $formInfo->cnpchannelgrp_groupname,
      '#attributes' => ['id' => 'hidden_fg_name'],
    ];
    $data = $this->getRecords();
    $options = [];
    foreach ($data as $rec) {
      $options[$rec->cnpstngs_AccountNumber . "||" . $rec->cnpstngs_guid . "||" . $rec->cnpstngs_ID] = $rec->cnpstngs_frndlyname . ' ( ' . $rec->cnpstngs_AccountNumber . ' )';
    }
    $form['cnpc_channel_save_url'] = [
      '#type' => 'hidden',
      '#default_value' => Url::fromRoute('cnpconnect.cnpc_save_channel_data', [])->toString(),
      '#attributes' => ['id' => 'cnpc_channel_save_url'],
    ];
    $form['base_url_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnpc'],
    ];
    $form['cnpc_edit_id'] = [
      '#type' => 'hidden',
      '#default_value' => $reqID,
      '#attributes' => ['id' => 'cnpc_edit_id'],
    ];
    $form['cnpc_action'] = [
      '#type' => 'hidden',
      '#default_value' => 'edit',
      '#attributes' => ['id' => 'cnpc_action'],
    ];
    $form['cnpc_accounts_hidden'] = [
      '#type' => 'hidden',
      '#default_value' => $formInfo->cnpstngs_AccountNumber,
      '#attributes' => ['id' => 'cnpc_accounts_hidden'],
    ];
    $form['cnpc_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Account(s)*'),
      '#options' => $options,
      '#default_value' => '',
      '#attributes' => ['id' => 'cnpc_accounts'],
      '#disabled' => TRUE,
    ];
    $form['cnpc_start_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Date/Time* @dateC', ['@dateC' => '[TimeZone: ' . date_default_timezone_get() . ']']),
      '#attributes' => ['id' => 'start_date_time'],
      '#default_value' => date('F d, Y h:i a', strtotime($formInfo->cnpchannelgrp_channel_StartDate)),
    ];
    if ($formInfo->cnpchannelgrp_channel_EndDate == '0000-00-00 00:00:00') {
      $ed = '';
    }
    else {
      $ed = date('F d, Y h:i a', strtotime($formInfo->cnpchannelgrp_channel_EndDate));
    }
    $form['cnpc_end_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End Date/Time'),
      '#attributes' => ['id' => 'end_date_time'],
      '#default_value' => $ed,
    ];
    $form['cnpc_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Valid Channel Message'),
      '#attributes' => ['id' => 'cnpc_message'],
      '#default_value' => $formInfo->cnpchannelgrp_custommsg,
    ];
    $form['cnpc_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        'active' => $this->t('Active'),
        'inactive' => $this->t('Inactive'),
      ],
      '#attributes' => ['id' => 'cnpc_status'],
      '#default_value' => $formInfo->cnpchannelgrp_status,
    ];
    // Hidden form Starts here.
    $noofFoms = count($formDetails);
    $form['cnpcchnl_counter'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'cnpcchnl_counter'],
      '#default_value' => $noofFoms,
    ];
    $form['save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'finalchnlsave'],
    ];
    $form['final_save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'updateforms'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Saving form data handling with ajax call.
  }

  /**
   * Get all the account settings details to display in Account drop down.
   *
   * @return array
   *   Returns an array of all accounts.
   */
  public function getRecords() {
    $sql = "SELECT cnpstngs_ID,cnpstngs_frndlyname,cnpstngs_AccountNumber,cnpstngs_guid,cnpstngs_status,cnpstngs_Date_Created,"
      . "cnpstngs_Date_Modified FROM {dp_cnpcsettingsdtl}";
    $query = $this->connection->query($sql);
    return $query->fetchAll();
  }

  /**
   * Get the channel information based channel group settings id.
   *
   * @param int $id
   *   Channel group settings id.
   *
   * @return array
   *   Returns an array of records which contains campaign forms forms.
   */
  public function getChannelInfo($id) {
    $sql = "SELECT * FROM {dp_cnpc_channelgrp} inner join {dp_cnpcsettingsdtl} on cnpchannelgrp_cnpstngs_ID = cnpstngs_ID where cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $id]);
    return $query->fetch();
  }

  /**
   * Get the channel details based on channel group id.
   *
   * @param int $id
   *   Channel group Id.
   *
   * @return array
   *   Returns channel details array.
   */
  public function getChannelDetails($id) {
    $sql = "SELECT cnpchannel_id,cnpchannel_cnpchannelgrp_ID,cnpchannel_channelName,cnpchannel_channelStartDate,"
      . "cnpchannel_channelEndDate,cnpchannel_channelStatus,cnpchannel_DateCreated,"
      . "cnpchannel_DateModified FROM {dp_cnpc_channeldtl} where cnpchannel_cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $id]);
    return $query->fetchAll();
  }

}
