
CONTENTS OF THIS FILE
---------------------

 * Quickstart
 * Requirements and notes
 * Optional server requirements
 * Installation
 * Reinstall
 * Building and customizing your site
 * Multisite configuration
 * Multilingual configuration

QUICKSTART
----------------------
Prerequisites:
- PHP 7.0.8 (or greater) (https://php.net).

- Drupal 8.0.0 above versions only

REQUIREMENTS AND NOTES
----------------------

Drupal requires:

- A web server with PHP support, for example:
  - Apache 2.0 (or greater) (http://httpd.apache.org/).
  - Nginx 1.1 (or greater) (http://nginx.com/).
- PHP 7.0.8 (or greater) (http://php.net/). For better security support it is
  recommended to update to at least 7.2.17.
- One of the following databases:
  - MySQL 5.5.3 (or greater) (http://www.mysql.com/).
  - MariaDB 5.5.20 (or greater) (https://mariadb.org/). MariaDB is a fully
    compatible drop-in replacement for MySQL.
  - Percona Server 5.5.8 (or greater) (http://www.percona.com/). Percona
    Server is a backwards-compatible replacement for MySQL.
  - PostgreSQL 9.1.2 (or greater) (http://www.postgresql.org/).
  - SQLite 3.7.11 (or greater) (http://www.sqlite.org/).
