<?php

namespace Drupal\clickandpledge_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CONNECT platform account settings form.
 *
 * Uses to create a form, that verifies AccoundID and GUID with CONNECT
 * platform after verification, it stores the data with nickname in table.
 */
class CnPAccountSettings extends FormBase {

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Connection Object.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnpc_account_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#theme'] = 'cnpc_settings_form';
    $displayForm = $this->displayAccountSettingsForm($form, $form_state);
    return $displayForm;
  }

  /**
   * Create a form to verify account details GUID and AccountID.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displayAccountSettingsForm(array $form, $form_state) {
    $form['cnpc_account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Click & Pledge Account Number*'),
      '#description' => $this->t('Please enter the Account Number of your CONNECT Account<br>[CONNECT > Launcher > Settings > API Information > Account ID]<br><span class="cnpc_exists_error"></span>'),
      '#default_value' => '',
      '#attributes' => ['id' => 'cnpc_account_number'],
    ];
    $form['cnpc_account_guid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Click & Pledge Account GUID*'),
      '#description' => $this->t('Please enter the Account <a href="https://support.clickandpledge.com/s/article/how-to-locate-account-id--api-account-guid" target="_blank">GUID</a> of your CONNECT Account<br>[CONNECT > Launcher > Settings > API Information > API (PaaS / FaaS): Account GUID].'),
      '#default_value' => '',
      '#attributes' => ['id' => 'cnpc_account_guid'],
    ];
    $form['base_url_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnpc'],
    ];
    $form['nickname_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => '',
      '#attributes' => ['id' => 'nickname_cnpc'],
    ];
    $form['response_cnpc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nickname*'),
      '#default_value' => '',
      '#attributes' => [
        'id' => 'response_cnpc',
        'disabled' => 'disabled',
      ],
      '#prefix' => '<div class="nickname-div">',
      '#suffix' => '</div>',
    ];
    $form['save'] = [
      '#type' => 'submit',
      '#input' => TRUE,
      '#value' => $this->t('Verify'),
    ];
    $form['page_break'] = [
      '#prefix' => '<div class="page_break">',
      '#suffix' => '</div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $accNumber = ($form_state->getValue('cnpc_account_number')) ? $form_state->getValue('cnpc_account_number') : "";
    $accGuid = ($form_state->getValue('cnpc_account_guid')) ? $form_state->getValue('cnpc_account_guid') : "";
    $nickname = ($form_state->getValue('nickname_cnpc')) ? $form_state->getValue('nickname_cnpc') : "";
    $nickname = trim($nickname);
    $accNumber = trim($accNumber);
    $accGuid = trim($accGuid);
    $table_name = 'dp_cnpcsettingsdtl';
    $this->connection->insert($table_name)
      ->fields([
        'cnpstngs_frndlyname',
        'cnpstngs_AccountNumber',
        'cnpstngs_guid',
      ])
      ->values([
        $nickname,
        $accNumber,
        $accGuid,
      ])
      ->execute();
  }

}
