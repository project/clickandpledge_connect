CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
Form and pledgeTV® groups may be defined as a family of forms or TV Channels,
from one or different campaigns,with different start & end dates.

* For a full description of the module, visit the project page:
https://manual.clickandpledge.com/Click-Pledge-Connect-Module-2.html

REQUIREMENTS
------------
This module does not requires any other modules, it can work independently.
This module needs the following information

* An active Click & Pledge Account. If you are not an existing account holder
please apply here. https://clickandpledge.com/sign-up/

* This plugin also requires the PHP extension SOAP to be installed 
and configured on the server where the website is hosted.
If you do not have this extension,contact your website hosting provider 
to add it.

* Secure site – Your web site domain name must have SSL configured & all request
from http should redirect to https.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module.Download 
the module from below link 
https://www.drupal.org/project/clickandpledge_connect

* This module can work in Drupal 8 & above.

CONFIGURATION
-------------
* The module has no menu or modifiable settings. There is no configuration. When
enabled, we can see a link under 
Configuration >> Development >> Click & Pledge CONNECT.

* By click the link we can get started with our module.
Please visit the below link.
https://manual.clickandpledge.com/Click-Pledge-Connect-Module-2.html
