<?php

namespace Drupal\clickandpledge_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Utility\Xss;

/**
 * This class handles all the AJAX calls to save and update the module data.
 */
class CnPController extends ControllerBase {

  /**
   * The constants.
   */
  const CFCNP_PLUGIN_UID = '14059359-D8E8-41C3-B628-E7E030537905';
  const CFCNP_PLUGIN_SKY = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
  const CFCNP_SOAP_VER = 'SOAP_1_1';
  const CFCNP_SERVICE_URL = 'https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl';

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The RequestStack Variable.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * The Class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Request stacke object to get the posted data.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler, RequestStack $request_stack) {
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('module_handler'), $container->get('request_stack')
    );
  }

  /**
   * AccountID and GUID by authenticating with CONNECT platform.
   *
   * GUID and AccountID with service URL,It will get the nickname for the
   * account. Based on the nickname CONNECT forms will displayed.
   *
   * @param mixed $variable1
   *   The account id.
   * @param mixed $variable2
   *   The account GUID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function ajaxCallback($variable1, $variable2) {
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client = new \SoapClient(self::CFCNP_SERVICE_URL, $connect);
    if (isset($variable1) && isset($variable2) && $variable1 != '' && $variable2 != '') {
      $accountid = $variable1;
      $accountguid = $variable2;
      $xmlr = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr->addChild('accountId', $accountid);
      $xmlr->addChild('accountGUID', $accountguid);
      $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
      $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
      $response = $client->GetAccountDetail($xmlr);
      $responsearr = $response->GetAccountDetailResult->AccountNickName;
      return new JsonResponse(['status' => $responsearr]);
    }
    else {
      return new JsonResponse(['status' => FALSE]);
    }
  }

  /**
   * GetAllCampaigns method will get all the campaigns based on nickname.
   *
   * @param string $variable
   *   The variable holds Account GUID and AccountID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAllCampaigns($variable) {
    $str = explode('@', $variable);
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    $accountguid = $str[1];
    $accountid = $str[0];
    $xmlr = new SimpleXMLElement('<GetActiveCampaignList2></GetActiveCampaignList2>');
    $xmlr->addChild('accountId', $accountid);
    $xmlr->addChild('AccountGUID', $accountguid);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $response = $client->GetActiveCampaignList2($xmlr);
    $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;
    $camrtrnval = '';
    $orderRes = [];
    if (count($responsearr) == 1) {
      $orderRes[$responsearr->alias] = $responsearr->name;
    }
    else {
      foreach ($responsearr as $obj) {
        $orderRes[$obj->alias] = $obj->name;
      }
    }
    natcasesort($orderRes);
    $camrtrnval .= '<option value="">' . $this->t('Select Campaign Name') . '</option>';
    if (count($orderRes) > 0) {
      foreach ($orderRes as $key => $value) {
        $camrtrnval .= "<option value='" . Xss::filter($key) . "'>" . Xss::filter($value) . " (" . Xss::filter($key) . ")</option>";
      }
    }
    return new JsonResponse(['data' => $camrtrnval]);
  }

  /**
   * Get all the forms based on accountID and GUID.
   *
   * This method depends on the response of getAllCampaigns method.
   * Because each campaign holds groups of forms.
   *
   * @param mixed $variable
   *   The variable holds AccoundID, Account GUID and Campaign ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON Response.
   */
  public function getActivityForms($variable) {
    $str = explode('@', $variable);
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    $cnpaccountID = $str[0];
    $cnpaccountguidID = $str[1];
    $cnpcampaignId = $str[2];
    $xmlr = new SimpleXMLElement('<GetActiveFormList2></GetActiveFormList2>');
    $xmlr->addChild('accountId', $cnpaccountID);
    $xmlr->addChild('AccountGUID', $cnpaccountguidID);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $xmlr->addChild('campaignAlias', $cnpcampaignId);
    $frmresponse = $client->GetActiveFormList2($xmlr);
    $frmresponsearr = $frmresponse->GetActiveFormList2Result->form;
    $orderRes = [];
    if (count($frmresponsearr) == 1) {
      $orderRes[$frmresponsearr->formGUID] = $frmresponsearr->formName;
    }
    else {
      foreach ($frmresponsearr as $obj) {
        $orderRes[$obj->formGUID] = $obj->formName;
      }
    }
    natcasesort($orderRes);
    $rtrnval = '<option value="">' . $this->t('Select Form Name') . '</option>';
    if (count($orderRes) > 0) {
      foreach ($orderRes as $key => $value) {
        $rtrnval .= "<option value='" . Xss::filter($key) . "'>" . Xss::filter($value) . "</option>";
      }
    }
    return new JsonResponse(['data' => $rtrnval]);
  }

  /**
   * Saves the CONNECT forms data for both Add New Form Group and Edit.
   *
   * Form Group and also generates the short code only for form adding based
   * on the details entered.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response which holds success and fail messages.
   */
  public function saveFormData() {
    date_default_timezone_set(date_default_timezone_get());
    $data = $this->requestStack->getCurrentRequest()->request->all();
    $module_path = $this->moduleHandler->getModule('clickandpledge_connect')->getPath();
    if ($this->requestStack->getCurrentRequest()->files->get('file')) {
      $filename = $this->requestStack->getCurrentRequest()->files->get('file')->getClientOriginalName();
      $tname = $this->requestStack->getCurrentRequest()->files->get('file')->getPathname();
      move_uploaded_file($tname, $module_path . "/banners/$filename");
    }
    else {
      if ($data['cnpc_action'] === 'edit') {
        if (in_array('file', $data)) {
          if ($data['file']) {
            $filename = $data['file'];
          }
          else {
            $filename = '';
          }
        }
        else {
          $filename = '';
        }
      }
      else {
        $filename = '';
      }
    }
    $cnpc_account_str = explode('||', $data['cnpc_account']);
    $guid = $cnpc_account_str[1];
    $accid = $cnpc_account_str[0];
    $refId = $cnpc_account_str[2];
    $display_type = $data['cnpc_display_type'];
    $link_type = (isset($data['link_type'])) ? $data['link_type'] : "";
    $button_label = (isset($data['button_label'])) ? $data['button_label'] : "";
   if( $data['text_label'] != "" && $data['button_label'] == ""){
    $button_label = (isset($data['text_label'])) ? $data['text_label'] : "";}
    if ($data['cnpc_end_datetime']) {
      $cnpc_end_datetime = date('Y-m-d h:i:s', strtotime($data['cnpc_end_datetime']));
    }
    else {
      $cnpc_end_datetime = '0000-00-00 00:00:00';
    }
    $cnpc_group_name = trim($data['cnpc_group_name']);
    $cnpc_group_name_shortcode = str_replace(' ', '-', $cnpc_group_name);
    $cnpc_message = $data['cnpc_message'];
    $cnpc_start_datetime = date("Y-m-d h:i:s", strtotime($data['cnpc_start_datetime']));
    $cnpc_status = $data['cnpc_status'];
    $shortcode = '[cnpform]' . $cnpc_group_name_shortcode . '[/cnpform]';
    $table_name1 = 'dp_cnpc_forminfo';
    $iID = [];
    if ($data['cnpc_action'] === 'add') {
      $query1 = $this->connection->insert($table_name1)
        ->fields([
          'cnpform_groupname',
          'cnpform_cnpstngs_ID',
          'cnpform_AccountNumber',
          'cnpform_guid',
          'cnpform_type',
          'cnpform_ptype',
          'cnpform_text',
          'cnpform_img',
          'cnpform_shortcode',
          'cnpform_custommsg',
          'cnpform_Form_StartDate',
          'cnpform_Form_EndDate',
          'cnpform_status',
          'cnpform_Date_Modified',
        ])
        ->values([
          $cnpc_group_name,
          $refId,
          $accid,
          $guid,
          $display_type,
          $link_type,
          $button_label,
          $filename,
          $shortcode,
          $cnpc_message,
          $cnpc_start_datetime,
          $cnpc_end_datetime,
          $cnpc_status,
          date('Y-m-d h:i:s'),
        ])
        ->execute();
    }
    else {
      $this->connection->update($table_name1)
        ->fields([
          'cnpform_groupname' => $cnpc_group_name,
          'cnpform_AccountNumber' => $accid,
          'cnpform_guid' => $guid,
          'cnpform_type' => $display_type,
          'cnpform_ptype' => $link_type,
          'cnpform_text' => $button_label,
          'cnpform_img' => $filename,
          'cnpform_shortcode' => $shortcode,
          'cnpform_custommsg' => $cnpc_message,
          'cnpform_Form_StartDate' => $cnpc_start_datetime,
          'cnpform_Form_EndDate' => $cnpc_end_datetime,
          'cnpform_status' => $cnpc_status,
          'cnpform_Date_Modified' => date('Y-m-d h:i:s'),
        ])
        ->condition('cnpform_GID', $data['cnpc_edit_id'])
        ->execute();
    }
    $table_name = 'dp_cnpc_formsdtl';
    if ($data['cnpc_action'] === 'edit') {
      $nid = $data['cnpc_edit_id'];

      $did = $data['cnpc_edit_id'];
      $this->connection->delete($table_name)
        ->condition('cnpform_cnpform_ID', $did)
        ->execute();
    }
    else {
      $nid = $query1;
    }
    // Insert form group information.
    $ls = explode('###', $data['cnpc_camp_list'], -1);
    $gu = explode('###', $data['formguid'], -1);
    $sd = explode('###', $data['startdate'], -1);
    $ed = explode('###', $data['enddate'], -1);
    $pl = explode('###', $data['params_list'], -1);
    $form_list_titles = explode('###', $data['form_list_titles'], -1);
    for ($i = 0; $i < count($ls); $i++) {
      $cnamp_list = $ls[$i];
      $guid_list = $gu[$i];
      if ($pl[$i]) {
        $param_list = $pl[$i];
      }
      else {
        $param_list = '';
      }
      $sd_list = date('Y-m-d h:i:s', strtotime($sd[$i]));
      if ($ed[$i]) {
        $ed_list = date('Y-m-d h:i:s', strtotime($ed[$i]));
      }
      else {
        $ed_list = '0000-00-00 00:00:00';
      }
      $flt = $form_list_titles[$i];
      $query = $this->connection->insert($table_name)
        ->fields([
          'cnpform_cnpform_ID',
          'cnpform_CampaignName',
          'cnpform_FormName',
          'cnpform_GUID',
          'cnpform_FormStartDate',
          'cnpform_FormEndDate',
          'cnpform_FormStatus',
          'cnpform_urlparameters',
        ])
        ->values([
          $nid,
          $cnamp_list,
          $flt,
          $guid_list,
          $sd_list,
          $ed_list,
          $cnpc_status,
          $param_list,
        ])
        ->execute();
      $iID[] = $query;
    }
    if (count($iID) > 0) {
      return new JsonResponse(['data' => 'success']);
    }
    else {
      return new JsonResponse(['data' => 'fail']);
    }
  }

  /**
   * Update the form status based on current status and form id.
   *
   * @param string $variableone
   *   The current form status that is either active or in active.
   * @param int $variabletwo
   *   Form ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponset
   *   JSON response to display success and fail messages.
   */
  public function updateFormStatus($variableone, $variabletwo) {
    if ($variableone === 'active') {
      $variableone = 'inactive';
    }
    else {
      $variableone = 'active';
    }
    $num_updated = $this->connection->update('dp_cnpc_forminfo')
      ->fields([
        'cnpform_status' => $variableone,
      ])
      ->condition('cnpform_GID', $variabletwo)
      ->execute();
    if ($num_updated) {
      return new JsonResponse(['data' => 'success']);
    }
    else {
      return new JsonResponse(['data' => 'fail']);
    }
  }

  /**
   * Deletes the form details by clicking delete icon in settings tab.
   *
   * If any form groups are assigned to the form, it wont be deleted.
   *
   * @param int $variable
   *   Form group ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the deleted record ID.
   */
  public function deleteFormDetails($variable) {
    $table_name = 'dp_cnpc_formsdtl';
    $num_deleted = $this->connection->delete($table_name)
      ->condition('cnpform_id', $variable)
      ->execute();
    return new JsonResponse([$num_deleted]);
  }

  /**
   * Deletes the form groups.
   *
   * @param int $variable
   *   The form group ID.
   */
  public function deleteFormGroup($variable) {
    $table_name = 'dp_cnpc_forminfo';
    $deleted_info = $this->connection->delete($table_name)
      ->condition('cnpform_GID', $variable)
      ->execute();
    if ($deleted_info) {
      $table_name1 = 'dp_cnpc_formsdtl';
      $this->connection->delete($table_name1)
        ->condition('cnpform_cnpform_ID', $variable)
        ->execute();
      $this->cnpcRedirect('../../cnp_form');
    }
    else {
      $this->cnpcRedirect('../../cnp_form');
    }
  }

  /**
   * Page redirection.
   *
   * @param string $path
   *   Path to redirect.
   */
  public function cnpcRedirect($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

  /**
   * Deletes the account settings details from settings tab.
   *
   * @param int $variable
   *   Settings ID from dp_cnpcsettingsdtl table.
   */
  public function deleteSettingsDetails($variable) {
    $sql = 'SELECT cnpform_groupname FROM {dp_cnpc_forminfo} where cnpform_cnpstngs_ID=:stngsid';
    $query = $this->connection->query($sql, [':stngsid' => $variable]);
    if (count($query->fetchAll()) == 0) {
      $deleted_dt1 = $this->connection->delete('dp_cnpcsettingsdtl')
        ->condition('cnpstngs_ID', $variable)
        ->execute();
      if ($deleted_dt1) {
        $this->cnpcRedirect('../../cnp_formssettings?msg=success');
      }
      else {
        $this->cnpcRedirect('../../cnp_formssettings?msg=error');
      }
    }
    else {
      $this->cnpcRedirect('../../cnp_formssettings?msg=associated');
    }
  }

  /**
   * It will get the updated data from CONNECT platform.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response to display success and fail messages after getting
   *   the updated data.
   */
  public function refreshAccounts() {
    $accid = $this->requestStack->getCurrentRequest()->request->get('saccid');
    $guid = $this->requestStack->getCurrentRequest()->request->get('sguid');
    $recid = $this->requestStack->getCurrentRequest()->request->get('srecid');
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client = new \SoapClient(self::CFCNP_SERVICE_URL, $connect);
    if (isset($accid) && isset($guid) && $accid !== '' && $guid !== '') {
      $accountid = $accid;
      $accountguid = $guid;
      $xmlr = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr->addChild('accountId', $accountid);
      $xmlr->addChild('accountGUID', $accountguid);
      $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
      $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
      $response = $client->GetAccountDetail($xmlr);
      $responsearr = $response->GetAccountDetailResult->AccountNickName;
      $table_name = 'dp_cnpcsettingsdtl';
      $updated = $this->connection->update($table_name)
        ->fields([
          'cnpstngs_frndlyname' => $responsearr,
        ])
        ->condition('cnpstngs_ID', $recid)
        ->execute();
      if ($updated) {
        return new JsonResponse(['status' => 'success']);
      }
      else {
        return new JsonResponse(['status' => 'fail']);
      }
    }
    else {
      return new JsonResponse(['status' => 'fail']);
    }
  }

  /**
   * Saves the PledgeTV settings form data for both Add and Edit cases.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response to display success and fail messages.
   */
  public function saveChannelData() {
    $data = $this->requestStack->getCurrentRequest()->request->all();
    date_default_timezone_set(date_default_timezone_get());
    if ($data['cnpc_action'] !== 'edit') {
      $cnpc_account_str = explode("||", $data['cnpc_account']);
      $refId = $cnpc_account_str[2];
    }
    if ($data['cnpc_end_datetime']) {
      $cnpc_end_datetime = date("Y-m-d h:i:s", strtotime($data['cnpc_end_datetime']));
    }
    else {
      $cnpc_end_datetime = "0000-00-00 00:00:00";
    }
    $chnlcnpc_group_name = trim($data['cnpc_group_name']);
    $chnlcnpc_group_name_shortcode = str_replace(' ', '-', $chnlcnpc_group_name);
    $cnpc_message = $data['cnpc_message'];
    $cnpc_start_datetime = date('Y-m-d h:i:s', strtotime($data['cnpc_start_datetime']));
    $cnpc_status = $data['cnpc_status'];
    $shortcode = "[cnppledgetv]" . $chnlcnpc_group_name_shortcode . "[/cnppledgetv]";
    $table_name1 = 'dp_cnpc_channelgrp';
    $ciID = [];
    if ($data['cnpc_action'] == 'add') {
      $query1 = $this->connection->insert($table_name1)
        ->fields([
          'cnpchannelgrp_groupname',
          'cnpchannelgrp_cnpstngs_ID',
          'cnpchannelgrp_shortcode',
          'cnpchannelgrp_custommsg',
          'cnpchannelgrp_channel_StartDate',
          'cnpchannelgrp_channel_EndDate',
          'cnpchannelgrp_status',
          'cnpchannelgrp_Date_Modified',
        ])
        ->values([
          $chnlcnpc_group_name,
          $refId,
          $shortcode,
          $cnpc_message,
          $cnpc_start_datetime,
          $cnpc_end_datetime,
          $cnpc_status,
          date('Y-m-d h:i:s'),
        ])
        ->execute();
    }
    else {
      $cnpc_account_str = explode('||', $data['cnpc_account']);
      $refId = $cnpc_account_str[2];
      $table_name1 = 'dp_cnpc_channelgrp';
      $this->connection->update($table_name1)
        ->fields([
          'cnpchannelgrp_groupname' => $chnlcnpc_group_name,
          'cnpchannelgrp_cnpstngs_ID' => $refId,
          'cnpchannelgrp_shortcode' => $shortcode,
          'cnpchannelgrp_custommsg' => $cnpc_message,
          'cnpchannelgrp_channel_StartDate' => $cnpc_start_datetime,
          'cnpchannelgrp_channel_EndDate' => $cnpc_end_datetime,
          'cnpchannelgrp_status' => $cnpc_status,
          'cnpchannelgrp_Date_Modified' => date('Y-m-d h:i:s'),
        ])
        ->condition('cnpchannelgrp_ID', $data['cnpc_edit_id'])
        ->execute();
    }
    $table_name = 'dp_cnpc_channeldtl';
    if ($data['cnpc_action'] === 'edit') {
      $nid = $data['cnpc_edit_id'];
      $did = $data['cnpc_edit_id'];
      $this->connection->delete($table_name)
        ->condition('cnpchannel_cnpchannelgrp_ID', $did)
        ->execute();
    }
    else {
      $nid = $query1;
    }
    // Insert Channel group information.
    $ls = explode('###', $data['cnpc_chnl_list'], -1);
    $sd = explode('###', $data['chnlstartdate'], -1);
    $ed = explode('###', $data['chnlenddate'], -1);
    for ($i = 0; $i < count($ls); $i++) {
      $cnamp_list = $ls[$i];
      $sd_list = date('Y-m-d h:i:s', strtotime($sd[$i]));
      if ($ed[$i]) {
        $ed_list = date('Y-m-d h:i:s', strtotime($ed[$i]));
      }
      else {
        $ed_list = '0000-00-00 00:00:00';
      }
      $cquery = $this->connection->insert($table_name)
        ->fields([
          'cnpchannel_cnpchannelgrp_ID',
          'cnpchannel_channelName',
          'cnpchannel_channelStartDate',
          'cnpchannel_channelEndDate',
          'cnpchannel_channelStatus',
        ])
        ->values([
          $nid,
          $cnamp_list,
          $sd_list,
          $ed_list,
          $cnpc_status,
        ])
        ->execute();
      $ciID[] = $cquery;
    }
    if (count($ciID) > 0) {
      return new JsonResponse(['data' => 'success']);
    }
    else {
      return new JsonResponse(['data' => 'fail']);
    }
  }

  /**
   * Delete channel information from pledgeTV.
   *
   * @param int $variable
   *   Channel Id indp_cnpc_channeldtl table.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return the Id of the deleted record.
   */
  public function deletechannelDetails($variable) {
    $num_deleted = $this->connection->delete('dp_cnpc_channeldtl')
      ->condition('cnpchannel_id', $variable)
      ->execute();
    return new JsonResponse([$num_deleted]);
  }

  /**
   * Delete channel forms.
   *
   * @param int $variable
   *   Channel group id from dp_cnpc_channeldtl table.
   */
  public function deletechannelGroup($variable) {
    $deleted_info = $this->connection->delete('dp_cnpc_channelgrp')
      ->condition('cnpchannelgrp_ID', $variable)
      ->execute();
    if ($deleted_info) {
      $this->connection->delete('dp_cnpc_channeldtl')
        ->condition('cnpchannel_cnpchannelgrp_ID', $variable)
        ->execute();
      $this->cnpcRedirect('../../CnPpledgeTVChannels');
    }
    else {
      $this->cnpcRedirect('../../CnPpledgeTVChannels');
    }
  }

  /**
   * Get the channel information based on GUID and AccountID.
   *
   * @param string $variable
   *   GUID and AccountId separated with @ symbol.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns a JSON string with all channels.
   */
  public function getAllChannels($variable) {
    $str = explode('@', $variable);
    $connect = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $module_path = $this->moduleHandler->getModule("clickandpledge_connect")->getPath() . '/Auth2.wsdl';
    $client = new \SoapClient($module_path, $connect);
    $camrtrnval = '';
    $caccountguid = $str[1];
    $caccountid = $str[0];
    $xmlr = new SimpleXMLElement('<GetPledgeTVChannelList></GetPledgeTVChannelList>');
    $xmlr->addChild('accountId', $caccountid);
    $xmlr->addChild('AccountGUID', $caccountguid);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $response = $client->GetPledgeTVChannelList($xmlr);
    $responsearr = $response->GetPledgeTVChannelListResult->PledgeTVChannel;
    $orderRes = [];
    if (count($responsearr) == 1) {
      $orderRes[$responsearr->ChannelURLID] = $responsearr->ChannelName;
    }
    else {
      foreach ($responsearr as $obj) {
        $orderRes[$obj->ChannelURLID] = $obj->ChannelName;
      }
    }
    natcasesort($orderRes);
    $camrtrnval .= "<option value=''>" . $this->t('Select channels') . "</option>";
    if (count($orderRes) > 0) {
      foreach ($orderRes as $key => $value) {
        $camrtrnval .= "<option value='" . Xss::filter($key) . "'>" . Xss::filter($value) . " (" . Xss::filter($key) . ")</option>";
      }
    }
    return new JsonResponse(['data' => $camrtrnval]);
  }

  /**
   * To update the form status(Active or Inactive).
   *
   * @param mixed $variable1
   *   Channel group status that is either active or inactive.
   * @param mixed $variable2
   *   Channel group Id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON response success on successful updation else fail
   */
  public function updatePtvformStatus($variable1, $variable2) {
    if ($variable1 === 'active') {
      $variable1 = 'inactive';
    }
    else {
      $variable1 = 'active';
    }
    $num_updated = $this->connection->update('dp_cnpc_channelgrp')
      ->fields([
        'cnpchannelgrp_status' => $variable1,
      ])
      ->condition('cnpchannelgrp_ID', $variable2)
      ->execute();
    if ($num_updated) {
      return new JsonResponse(['data' => 'success']);
    }
    else {
      return new JsonResponse(['data' => 'fail']);
    }
  }

  /**
   * Delete the forms details based on form id.
   *
   * @param int $variable
   *   Channel Id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns deleted record Id.
   */
  public function deleteTvFormDetails($variable) {
    $num_deleted = $this->connection->delete('dp_cnpc_channeldtl')
      ->condition('cnpchannel_id', $variable)
      ->execute();
    return new JsonResponse([$num_deleted]);
  }

  /**
   * Verifies the account based on account number.
   *
   * @param int $variable
   *   Account Number.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns no.of records.
   */
  public function accountVerification($variable) {
    $sql = 'SELECT cnpstngs_frndlyname,cnpstngs_AccountNumber FROM {dp_cnpcsettingsdtl} where cnpstngs_AccountNumber=:accnum';
    $query = $this->connection->query($sql, [':accnum' => $variable]);
    return new JsonResponse(count($query->fetchAll()));
  }

  /**
   * Verifies the form group based on form group name.
   *
   * @param string $variable1
   *   From group name.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns total no.of records.
   */
  public function formGroupVerification($variable1) {
    $sql = 'SELECT cnpform_groupname FROM {dp_cnpc_forminfo} where cnpform_groupname=:grpname';
    $query = $this->connection->query($sql, [':grpname' => $variable1]);
    return new JsonResponse(count($query->fetchAll()));
  }

  /**
   * Verifies the channel information based on channel group name.
   *
   * @param string $variable
   *   Channel group name.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns no.of records.
   */
  public function channelGroupVerification($variable) {
    $sql = "SELECT cnpchannelgrp_groupname FROM {dp_cnpc_channelgrp} where cnpchannelgrp_groupname=:groupname";
    $query = $this->connection->query($sql, [':groupname' => $variable]);
    return new JsonResponse(count($query->fetchAll()));
  }

}
