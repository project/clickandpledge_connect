<?php

namespace Drupal\clickandpledge_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Uses to create CONNECT forms.
 */
class AddFormGroup extends FormBase {

  /**
   * The Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The Class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnpcaccount_add_form_group';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#theme'] = 'cnp_add_form_group';
    $displayForm = $this->displayAccountSettingsForm($form, $form_state);
    return $displayForm;
  }

  /**
   * Creates a CONNECT form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displayAccountSettingsForm(array $form, $form_state) {
    date_default_timezone_set(date_default_timezone_get());
    $form['cnpc_form_group_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Group Name*'),
      '#description' => $this->t('Please enter the form group name<br><span class="cnpc_formexists_error"></span>'),
      '#default_value' => '',
      '#attributes' => ['id' => 'cnpc_form_group_name'],
    ];
    $data = $this->getRecords();
    if (count($data) > 0) {
      if (count($data) == 1) {
        $disabled = TRUE;
      }
      else {
        $disabled = FALSE;
      }
    }
    else {
      $disabled = FALSE;
    }
    $options = [];
    foreach ($data as $rec) {
      $options[trim($rec->cnpstngs_AccountNumber) . "||" . trim($rec->cnpstngs_guid) . "||" . trim($rec->cnpstngs_ID)] = $rec->cnpstngs_frndlyname . ' ( ' . $rec->cnpstngs_AccountNumber . ' ) ';
    }
    $form['base_url_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnpc'],
    ];
    $form['cnpc_form_save_url'] = [
      '#type' => 'hidden',
      '#default_value' => Url::fromRoute('cnpconnect.cnpc_save_form_data', [])->toString(),
      '#attributes' => ['id' => 'cnpc_form_save_url'],
    ];
    $form['cnpc_action'] = [
      '#type' => 'hidden',
      '#default_value' => 'add',
      '#attributes' => ['id' => 'cnpc_action'],
    ];
    $form['cnpc_params_textarea'] = [
      "#type" => "textarea",
      "#atrributes" => ['class' => ['paramsfeild']],
    ];
    $form['cnpc_params_textarea']['#theme_wrappers'] = [];
    $form['cnpc_params_button_add'] = [
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#attributes' => [
        'class' => ['popupsavebtn'],
        'id' => 'addparams',
      ],
    ];
    $form['cnpc_params_button_close'] = [
      '#type' => 'button',
      '#value' => $this->t('Close'),
      '#attributes' => [
        'class' => ['popupclosebtn'],
        'id' => 'closeparams',
      ],
    ];
    $form['cnpc_params_button_saveclose'] = [
      '#type' => 'button',
      '#value' => $this->t('Add & Close'),
      '#attributes' => [
        'class' => ['popupsaveclosebtn'],
        'id' => 'savecloseparams',
      ],
    ];
    // URL Params popup Ended.
    $form['cnpc_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Account(s)*'),
      '#options' => $options,
      '#attributes' => ['id' => 'cnpc_accounts'],
      '#disabled' => $disabled,
    ];
    $form['cnpc_start_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Date/Time* @dateC', ['@dateC' => '[TimeZone: ' . date_default_timezone_get() . ']']),
      '#attributes' => ["id" => "start_date_time"],
      '#default_value' => date('F d, Y h:i a'),
    ];
    $form['cnpc_end_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End Date/Time'),
      '#attributes' => ['id' => 'end_date_time'],
      '#default_value' => '',
    ];
    $form['cnpc_display_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Type'),
      '#options' => [
        'inline' => 'Inline',
        'popup' => 'Overlay',
      ],
      '#attributes' => ['id' => 'cnpc_display_type'],
    ];
    // Hidden form overlay.
    $form['cnpc_link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Link Type*'),
      '#options' => [
        'button' => $this->t('Button'),
        'image' => $this->t('Image'),
        'text' => $this->t('Text'),
      ],
      '#attributes' => ['id' => 'cnpc_link_type'],
    ];
    $form['cnpc_buton_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Label*'),
      '#attributes' => ['id' => 'cnpc_btn_lbl'],
      '#default_value' => '',
    ];
  $form['cnpc_text_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text Label*'),
      '#attributes' => ['id' => 'cnpc_txt_lbl'],
      '#default_value' => '',
    ];
    $form['cnpc_upload_image'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload Image*'),
      '#attributes' => ['id' => 'cnpc_upload_image'],
      '#default_value' => '',
    ];
    $form['cnpc_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Valid Form Message'),
      '#attributes' => ['id' => 'cnpc_message'],
      '#default_value' => $this->t('No donations are accepted at this time'),
    ];
    $form['cnpc_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        'active' => 'Active',
        'inactive' => 'Inactive',
      ],
      '#attributes' => ['id' => 'cnpc_status'],
    ];
    $form['cnpc_counter'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'cnpc_counter'],
      '#default_value' => 0,
    ];
    $form['save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'savebtn'],
    ];
    $form['final_save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'finalsave'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Form data is saving with ajax calls.
  }

  /**
   * Get all the accounts and displayed on accounts drop down in the form.
   *
   * @return array
   *   Returns an array of all account details.
   */
  public function getRecords() {
    $sql = 'SELECT cnpstngs_ID,cnpstngs_frndlyname,cnpstngs_AccountNumber,cnpstngs_guid,cnpstngs_status,cnpstngs_Date_Created,cnpstngs_Date_Modified FROM {dp_cnpcsettingsdtl}';
    $query = $this->connection->query($sql);
    return $query->fetchAll();
  }

}
