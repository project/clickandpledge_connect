<?php

namespace Drupal\clickandpledge_connect\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to help celebrate good times.
 *
 * @Filter(
 *   id = "filter_clickandpledge",
 *   title = @Translation("Click & Pledge Shortcode"),
 *   description = @Translation("Easy to add process for adding Connect forms and pledgeTV"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class Filterclickandpledge extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * The Class Constructor constructs Filterclickandpledge object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param mixed $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
 
    $cnpformshortcode = $this->settings['cnpshortcode_cnpform'] ? 'yes' : '';
    $cnppledgetvshortcode = $this->settings['cnpshortcode_cnppledgeTV'] ? 'yes' : '';
    $output = '';  
    $cnpsplitcntnt = preg_split('!(\[{1,2}.*?\]{1,2})!', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
    // PledgeTV Code Start.
    $mpos = array_search('[cnppledgetv]', $cnpsplitcntnt);
    if ($cnppledgetvshortcode != '' && $mpos != '') {
      $shortcode_tokens = &drupal_static(__FUNCTION__);
      $mpos = '';
      $cnporiginaltext = '';
      foreach ($cnpsplitcntnt as $cntnt) {
        if (!$cntnt) {
          continue;
        }
        if (($cntnt[0] == '[') && (substr($cntnt, -1, 1) == ']')) {
          $mpos = array_search('[cnppledgetv]', $cnpsplitcntnt);
          if ($mpos != '') {
            $srchshortcode = $cnpsplitcntnt[$mpos] . $cnpsplitcntnt[$mpos + 1] . $cnpsplitcntnt[$mpos + 2];
          }
          $chkshortcodexit = $this->cnpdpConnectIsExistchannelShortcode($srchshortcode);
          $cnporiginaltext = $srchshortcode;
          $mpostn = $mpos;
          $cnpgrpname = str_replace('-', ' ', $cnpsplitcntnt[$mpostn + 1]);
          if ($chkshortcodexit) {
            $cnpchannelid = $this->cnpdpConnectgetchannelsofGroup($cnpgrpname);
            $cntnt = substr($cntnt, 1, -1);
            $cntnt = trim($cntnt);
            $cnpts = explode(' ', $cntnt);
            $cnptag = array_shift($cnpts);
            if ($cnptag === 'cnppledgetv') {
              if (count($cnpchannelid) >= 1) {
                $rtrnstrarr = '';
                for ($frminc = 0; $frminc < count($cnpchannelid); $frminc++) {
                  $attrs = [
                    'class' => 'cnp_pledgetv_wrapper',
                    'data-channel' => $cnpchannelid[$frminc],
                    'data-iframe-width' => '100%',
                    'data-iframe-height' => '315',
                  ];
                  $attrs_string = '';
                  if (!empty($attrs)) {
                    foreach ($attrs as $key => $value) {
                      $attrs_string .= "$key='" . $value . "' ";
                    }
                    $attrs = ltrim($attrs_string);
                  }
                   $rtrnstr = " <script src='https://code.jquery.com/jquery-3.6.0.min.js'
  integrity='sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4='
  crossorigin='anonymous'></script><script> 
  var list = document.getElementsByTagName('script');
				var i = list.length, flag = false;
				while (i--) {
					if (list[i].src === 'https://pledge.tv/library/js/pledgetv.js') {
						flag = true;
						break;
					}
				}
				if (!flag) {
					var tag = document.createElement('script');
					tag.src = 'https://pledge.tv/library/js/pledgetv.js';
					document.getElementsByTagName('body')[0].appendChild(tag);
				}</script>";
                }
                $rtrnstrarr .= '<div ' . $attrs . '></div>';
             
              }
              else {
                $rtrnstrarr = $this->cnpconnectGroupchnlCustomerrmsg($srchshortcode);
              }
            //  $output .= $rtrnstrarr;
             $output = $rtrnstr . " " . $rtrnstrarr;
            }
          }
          else {
            $output = $this->cnpconnectGroupchnlCustomerrmsg($srchshortcode);
          }
        }
      }
    }
    // PledgeTV Code End.
    $fmpos = array_search('[cnpform]', $cnpsplitcntnt);
    $srchshortcodearr = [];
  
    if ($cnpformshortcode != '' && $fmpos != '') {
      $shortcode_tokens = &drupal_static(__FUNCTION__);
      $mpos = '';
      $cnpgrpname = '';
      $cnporiginaltext = '';
      $uid = $cnpsplitcntnt;
      $indexes = array_keys($uid, '[cnpform]');
      for ($sinc = 0; $sinc < count($indexes); $sinc++) {
        $mpos = $indexes[$sinc];
        $srchshortcode = $cnpsplitcntnt[$mpos] . $cnpsplitcntnt[$mpos + 1] . $cnpsplitcntnt[$mpos + 2];
        array_push($srchshortcodearr, $srchshortcode);
      }

      for ($sinc = 0; $sinc < count($srchshortcodearr); $sinc++) {

        $srchshortcode = $srchshortcodearr[$sinc];

        $chkshortcodexit = $this->cnpdprConnectIsExistShortcode($srchshortcode);
        $cnporiginaltext = $srchshortcode;
        $mpostn = $indexes[$sinc];
        $cnpgrpname = str_replace('-', ' ', $cnpsplitcntnt[$mpostn + 1]);
        if ($chkshortcodexit) {
          $cnpformid = $this->cnpdpcfGetformsofGroup($cnpgrpname);

          $cnpformtyp = $this->cnpdpcfGetFormType($cnpgrpname);
          $cntnt = substr($cnpsplitcntnt[$mpostn], 1, -1);
          $cntnt = trim($cntnt);
          $cnpts = explode(' ', $cntnt);
          $cnptag = array_shift($cnpts);
          if ($cnptag == 'cnpform') {
            if (count($cnpformid) >= 1) {
              $rtrnstrarr = '';
              for ($frminc = 0; $frminc < count($cnpformid); $frminc++) {

                $attrs = ['data-guid' => $cnpformid[$frminc]];
                $attrs_string = '';
                if (!empty($attrs)) {
                  foreach ($attrs as $key => $value) {
                    $attrs_string .= "$key='" . ($value) . "' ";
                  }
                  $attrs = ltrim($attrs_string);
                }
                $cnpshortcodearray = explode("--", $cnpformtyp);
                if ($cnpshortcodearray[0] == 'inline') {
                  $rtrnstrarr .= '<div class="CnP_inlineform" ' . $attrs . '></div>';
                }
                elseif ($cnpshortcodearray[0] == 'popup') {
                  if ($cnpshortcodearray[1] == 'text') {
                    $cnpGetImagesql = $cnpshortcodearray[2];
                    $rtrnstrarr .= '<a class="CnP_formlink" data-guid="' . $cnpformid[$frminc] . '">' . $cnpGetImagesql . '</a>';
                  }
                  elseif ($cnpshortcodearray[1] == 'button') {
                    $cnpGetbuttontext = $cnpshortcodearray[2];
                    $rtrnstrarr .= '<input class="CnP_formlink" type="submit" value="' . $cnpGetbuttontext . '" data-guid="' . $cnpformid[$frminc] . '" />';
                  }
                  elseif ($cnpshortcodearray[1] == 'image') {
                    $cnpGetImage = $cnpshortcodearray[3];
                    $module_path = $this->moduleHandler->getModule("clickandpledge_connect")->getPath() . "/banners/" . $cnpGetImage;
                    $rtrnstrarr .= '<img class="CnP_formlink " src="' . file_create_url($module_path) . '" data-guid="' . $cnpformid[$frminc] . '" style="cursor: pointer;">';
                  }
                }
                $rtrnstr = "<script src='https://code.jquery.com/jquery-3.6.0.min.js'
  integrity='sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4='
  crossorigin='anonymous'></script><script>
                var list = document.getElementsByTagName('script');
				var i = list.length, flag = false;
				while (i--) {
					if (list[i].src === 'https://resources.connect.clickandpledge.com/Library/iframe-1.0.0.min.js') {
						flag = true;
						break;
					}
				}
				if (!flag) {
					var tag = document.createElement('script');

					tag.class ='CnP_formloader';
					tag.src = 'https://resources.connect.clickandpledge.com/Library/iframe-1.0.0.min.js';
					document.getElementsByTagName('body')[0].appendChild(tag);
				}</script>";

              } $output = $rtrnstr . " " . $rtrnstrarr;
              $text = str_replace($srchshortcodearr[$sinc], $output, $text);

            }
            else {
              $output = $this->cnpdrcfGetGroupCustomerrmsg($cnpgrpname);
            }
          }
        }
        else {
          $output = $this->cnpdrcfGetGroupCustomerrmsg($cnpgrpname);
        }

      }
    }
    if (!empty($srchshortcodearr[0])) {
      $text = str_replace($srchshortcodearr[0], "", $text);
    }

    $cnpnewtext = str_replace($cnporiginaltext, $output, $text);
    $result = new FilterProcessResult($cnpnewtext);

    return $result;
  }

  /**
   * To get the custom message of channel group.
   *
   * @param string $cnpshortcode
   *   Short code.
   *
   * @return string
   *   Returns Custom message to display to users if the forms are expired.
   */
  public function cnpconnectGroupchnlCustomerrmsg($cnpshortcode) {
    $cnprtrnstr = "";
    $cnpGetImagesql = "SELECT cnpchannelgrp_custommsg FROM {dp_cnpc_channelgrp} WHERE cnpchannelgrp_shortcode =:shortcode";
    $query = $this->connection->query($cnpGetImagesql, [':shortcode' => $cnpshortcode]);
    $allcnpchannelgrps = $query->fetchAll();
    $cnpformrows = count($allcnpchannelgrps);
    if ($cnpformrows != NULL) {
      foreach ($allcnpchannelgrps as $cnpresultsarr) {
        $cnprtrnstr = $cnpresultsarr->cnpchannelgrp_custommsg;
      }
    }
    return $cnprtrnstr;
  }

  /**
   * To get all the channels based on group name.
   *
   * @param string $groupname
   *   Form group name.
   *
   * @return array
   *   Gets all the active forms as an array.
   */
  public function cnpdpConnectgetchannelsofGroup($groupname) {
    $currentdate = date('Y-m-d H:i:00');
    $returnarr = [];
    $scnpSQL = "SELECT cnpchannelgrp_ID as chnlid FROM {dp_cnpc_channelgrp} WHERE cnpchannelgrp_groupname = :groupname AND
	cnpchannelgrp_status ='active' AND IF (cnpchannelgrp_channel_EndDate !='0000-00-00 00:00:00', :currentdate between cnpchannelgrp_channel_StartDate and cnpchannelgrp_channel_EndDate, cnpchannelgrp_channel_StartDate <=  :currentdate) order by cnpchannelgrp_Date_Modified DESC Limit 1";
    $query = $this->connection->query($scnpSQL, [':groupname' => $groupname, ':currentdate' => $currentdate]);
    $allcnpchannelgrps = $query->fetchAll();
    $cnpformrows = count($allcnpchannelgrps);
    if ($cnpformrows != NULL) {
      foreach ($allcnpchannelgrps as $cnpresultsarr) {
        $cnpfrmid = $cnpresultsarr->chnlid;
      }
      $scnpFormsSQL = "SELECT cnpchannel_channelName as chnlnm FROM {dp_cnpc_channeldtl} WHERE cnpchannel_cnpchannelgrp_ID = :cnpfrmid AND cnpchannel_channelStatus ='active' AND   IF (cnpchannel_channelEndDate !='0000-00-00 00:00:00',  :currentdate between cnpchannel_channelStartDate and cnpchannel_channelEndDate, cnpchannel_channelStartDate <=  :currentdate) order by cnpchannel_DateCreated DESC Limit 1";
      $query = $this->connection->query($scnpFormsSQL, [':cnpfrmid' => $cnpfrmid, ':currentdate' => $currentdate]);
      $cnpformsresults = $query->fetchAll();
      $cnpformrows = count($cnpformsresults);
      if ($cnpformrows != NULL) {
        foreach ($cnpformsresults as $cnpfrmresultsarr) {
          array_push($returnarr, $cnpfrmresultsarr->chnlnm);
        }
      }
    }
    return $returnarr;
  }

  /**
   * To check channel short code exists or not.
   *
   * @param string $argcnpshortcode
   *   The Short code.
   *
   * @return bool
   *   If short code exists returns true else returns false.
   */
  public function cnpdpConnectIsExistchannelShortcode($argcnpshortcode) {
    $currentdate = date('Y-m-d H:i:00');
    $cnpGetImagesql = "SELECT * FROM {dp_cnpc_channelgrp} WHERE (cnpchannelgrp_shortcode =:shortcode) AND cnpchannelgrp_status ='active' AND IF (cnpchannelgrp_channel_EndDate !='0000-00-00 00:00:00', :currentdate between cnpchannelgrp_channel_StartDate and cnpchannelgrp_channel_EndDate, cnpchannelgrp_channel_StartDate <= :currentdate) order by cnpchannelgrp_Date_Modified ASC Limit 1";
    $query = $this->connection->query($cnpGetImagesql, [':shortcode' => $argcnpshortcode, ':currentdate' => $currentdate]);
    $allcnpchannelgrps = $query->fetchAll();
    if (count($allcnpchannelgrps) > 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the type of form,image and text.
   *
   * @param string $groupname
   *   The Channel group name.
   *
   * @return string
   *   Returns a string contains form type, form text and form message.
   */
  public function cnpdpcfGetFormType($groupname) {
    $currentdate = date('Y-m-d H:i:00');
    $cnpGetguidsql = "SELECT cnpform_type,cnpform_ptype,cnpform_text,cnpform_img FROM {dp_cnpc_forminfo} WHERE cnpform_groupname =:groupname AND cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', :currentdate between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <= :currentdate) order by cnpform_Date_Modified DESC Limit 1";
    $cnpfrmcntresultarry = $this->connection->query($cnpGetguidsql, [':groupname' => $groupname, ':currentdate' => $currentdate]);
    $cnpfrmcntresult = $cnpfrmcntresultarry->fetchAll();
    foreach ($cnpfrmcntresult as $cnpresultsarr) {
      $cnpform_accountId = $cnpresultsarr->cnpform_type;
      return $cnpform_accountId . "--" . $cnpresultsarr->cnpform_ptype . "--" . $cnpresultsarr->cnpform_text . "--" . $cnpresultsarr->cnpform_img;
    }
  }

  /**
   * To get all the forms  based on form group.
   *
   * @param string $groupname
   *   Channel group name.
   *
   * @return array
   *   Returns an array form groups.
   */
  public function cnpdpcfGetformsofGroup($groupname) {
    $returnarr = [];
    $currentdate = date('Y-m-d H:i:00');
    $scnpSQL = "SELECT cnpform_GID as frmid FROM {dp_cnpc_forminfo} WHERE cnpform_groupname = :groupname AND cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', :currentdate between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <=  :currentdate) order by cnpform_Date_Modified DESC Limit 1";
    $cnpquery = $this->connection->query($scnpSQL, [':groupname' => $groupname, ':currentdate' => $currentdate]);
    $cnpallForms = $cnpquery->fetchAll();
    $cnpformrows = count($cnpallForms);
    if ($cnpformrows != '') {
      foreach ($cnpallForms as $cnpresultsarr) {
        $cnpfrmid = $cnpresultsarr->frmid;
      }
      $scnpFormsSQL = "SELECT cnpform_GUID as frmguid,cnpform_urlparameters FROM {dp_cnpc_formsdtl} WHERE cnpform_cnpform_ID = :cnpfrmid AND cnpform_FormStatus ='active' AND   IF (cnpform_FormEndDate !='0000-00-00 00:00:00',  :currentdate between cnpform_FormStartDate and cnpform_FormEndDate, cnpform_FormStartDate <=  :currentdate) order by cnpform_DateCreated DESC Limit 1";
      $cnpfrmsquery = $this->connection->query($scnpFormsSQL, [':cnpfrmid' => $cnpfrmid, ':currentdate' => $currentdate]);
      $cnpguid = $cnpfrmsquery->fetchAll();
      $cnpformrows = count($cnpguid);
      if ($cnpformrows != NULL) {
        foreach ($cnpguid as $cnpfrmresultsarr) {
          $cnpurlparam = $cnpfrmresultsarr->cnpform_urlparameters;
          $cnpurlparamnw = str_replace(';', '&', $cnpurlparam);
          $newfrmguid = $cnpfrmresultsarr->frmguid . "?" . $cnpurlparamnw;
          array_push($returnarr, $newfrmguid);
        }
      }
    }
    return $returnarr;
  }

  /**
   * To get the customer message.
   *
   * @param string $cnpshortcode
   *   Form group short code.
   *
   * @return string
   *   Return a custom message for the form group.
   */
  public function cnpdrcfGetGroupCustomerrmsg($cnpshortcode) {
    $cnprtrnstr = '';
    $cnpGetImagesql = "SELECT cnpform_custommsg FROM {dp_cnpc_forminfo} WHERE cnpform_groupname =:shortcode";
    $cnperrresultqry = $this->connection->query($cnpGetImagesql, [':shortcode' => $cnpshortcode]);
    $cnperrresults = $cnperrresultqry->fetchAll();
    if (count($cnperrresults) > 0) {
      foreach ($cnperrresults as $cnperrresultsarr) {
        $cnprtrnstr = $cnperrresultsarr->cnpform_custommsg;
      }
    }
    return $cnprtrnstr;
  }

  /**
   * Function to check short code is exists or not.
   *
   * @param string $argcnpshortcode
   *   Form group short code.
   *
   * @return bool
   *   Returns true if short code exists else returns false.
   */
  public function cnpdprConnectIsExistShortcode($argcnpshortcode) {
    $currentdate = date('Y-m-d H:i:00');
    $cnpGetImagesql = "SELECT * FROM {dp_cnpc_forminfo} WHERE (cnpform_shortcode =:shortcode) AND cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', :currentdate between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <= :currentdate) order by cnpform_Date_Modified ASC Limit 1";
    $query = $this->connection->query($cnpGetImagesql, [':shortcode' => $argcnpshortcode, 'currentdate' => $currentdate]);
    $allcnpFormsgrps = $query->fetchAll();
    if (count($allcnpFormsgrps) > 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    if (count($this->settings) > 0) {
      $cnpformValue = $this->settings['cnpshortcode_cnpform'];
      $cnppledgetvValue = $this->settings['cnpshortcode_cnppledgeTV'];
    }
    else {
      $cnpformValue = '';
      $cnppledgetvValue = '';
    }
    $form['cnpshortcode_cnpform'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CONNECT Form Shortcode?'),
      '#default_value' => $cnpformValue,
      '#description' => $this->t('Easy to add process for adding Connect forms.'),
    ];
    $form['cnpshortcode_cnppledgeTV'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable pledgeTV Shortcode?'),
      '#default_value' => $cnppledgetvValue,
      '#description' => $this->t('Easy to add process for adding PledgeTV.'),
    ];
    return $form;
  }

}
