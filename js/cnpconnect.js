/**
 * @file
 * Handles asynchronous requests,validations,saving and updating data.
 *
 * CONNECT form Groups and pledgeTV channel groups.
 */

/**
 * @param {type} $
 * @param {type} Drupal
 * @returns {undefined}
 */
(function ($, Drupal) {
  // Code that uses jQuery's $ can follow here.
  'use strict';
  $('#cnpc-account-settings-form').find('input[type="submit"]').val('Verify');
  $('.nickname-div').hide();

  /**
   * This event handles the Click & Pledge account Form.
   *
   * It will authenticate the account id and GUID with CONNECT platform
   * after successful authentication it will gives a nickname or friendly name
   * all the information guid,accountId, nickname will be saved in table.
   */
  $('#cnpc-account-settings-form').find('input[type="submit"]').click((e) => {
    if ($('#response_cnpc').val() === '') {
      e.preventDefault();
    }
    const accGuid = $('#cnpc_account_guid').val();
    const accNum = $('#cnpc_account_number').val();
    if (accNum.trim() === '') {
      $('#cnpc_account_number').css('border', '1px solid red');
      return false;
    }
    if (accGuid.trim() === '') {
      $('#cnpc_account_guid').css('border', '1px solid red');
      return false;
    }
    if (accGuid.trim() !== '' && accNum.trim() !== '') {
      const basePath = $('#base_url_cnpc').val();
      const url = `${location.protocol}//${location.host}${basePath}admin/cnpconnect/check/${accNum}/${accGuid}`;
      $.ajax({
        url,
        type: 'GET',
        success(res) {
          if (res.status !== null) {
            $('.nickname-div').show();
            $('#response_cnpc').val(res.status);
            $('#nickname_cnpc').val(res.status);
            $('#cnpc-account-settings-form').find('input[type="submit"]').val('Save');
          }
          else {
            alert('Unable to find account details');
            $('.nickname-div').hide();
            $('#response_cnpc').val('');
            $('#nickname_cnpc').val('');
            $('#cnpc-account-settings-form').find('input[type="submit"]').val('Verify');
          }
        }
      });
    }
  });

  /**
   * To hide the error information, when both fields are filled with values.
   */
  $('#cnpc_account_guid').blur(function () {
    if ($(this).val().trim() !== '') {
      $('#cnpc_account_guid').removeAttr('style');
    }
  });
  $('#cnpc_account_number').blur(function () {
    if ($(this).val().trim() !== '') {
      $('#cnpc_account_number').removeAttr('style');
    }
  });

  /**
   * Actions to be performed when document loads.
   */
  $(document).ready(() => {
    $('#table_id').DataTable();
    $('#table_form_group').DataTable();
    $('#table_viewforms').DataTable();
    $('.cnpc-edit-channel-group').attr('autocomplete', 'off');
    $('.cnpcaccount-add-channel-group').attr('autocomplete', 'off');
    $('.cnpcaccount-add-form-group').attr('autocomplete', 'off');
    $('.cnpc-edit-form-group').attr('autocomplete', 'off');
    $('.cnpc-account-settings-form').attr('autocomplete', 'off');
    if ($('#cnpc_action').val() === 'edit') {
      $('.cnpcstartdate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.cnpcenddate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.chnlstartdate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.chnlenddate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.cnpc_form_list').each(function () {
        const opttext = $(this).data('opttext');
        const elementID = $(this).attr('id');
        $(`#${elementID} option`).each(function () {
          if ($(this).text() === opttext) {
            $(this).attr('selected', 'selected');
          }
          if ($(this).text() === 'Select Form Name') {
            $(this).val('');
          }
        });
      });
      $('.cnpc_camp_list').each(function () {
        const opttext = $(this).data('opttext');
        const elementID = $(this).attr('id');
        $(`#${elementID} option`).each(function () {
          if ($(this).val() === opttext) {
            $(this).attr('selected', 'selected');
          }
        });
        var Otext1;
        $(`#${elementID} option`).each(function () {
          if ($(this).text() === 'Select Campaign Name') {
            Otext1 = $(this).text();
            $(this).val('');
          }
          else {
            Otext1 = `${$(this).text()} (${$(this).val()})`;
          }
          $(this).text(Otext1);
        });
      });
      $('.cnpc_camp_list').each(function () {
        if ($(this).val() === '') {
          $(`#cnpc_form_list_${$(this).attr('data-rowindex')}`).val('');
          $(`#cnpc_form_list_${$(this).attr('data-rowindex')} option`).remove();
          $(`#formguid_${$(this).attr('data-rowindex')}`).val('');
        }
      });
      $('.cnpc_chnl_list').each(function () {
        const opttext = $(this).data('opttext');
        const elementID = $(this).attr('id');
        $(`#${elementID} option`).each(function () {
          if ($(this).val() === opttext) {
            $(this).attr('selected', 'selected');
          }
        });
        var Otext;
        $(`#${elementID} option`).each(function () {
          if ($(this).text() === 'Select Channel Name') {
            Otext = $(this).text();
            $(this).val('');
          }
          else {
            Otext = `${$(this).text()} (${$(this).val()})`;
          }
          $(this).text(Otext);
        });
      });
      const hidden_accid = $('#cnpc_accounts_hidden').val();
      $('#cnpc_accounts').each(function () {
        const elementID = $(this).attr('id');
        $(`#${elementID} option`).each(function () {
          const hid = $(this).val().split('||');
          if (hid[0] === hidden_accid) {
            $(this).attr('selected', 'selected');
          }
        });
      });

      if ($('#custrows tr').length - 1 >= 2) {
        $('.deleteRec').show();
      }
      if ($('#custchnlrows tr').length - 1 >= 2) {
        $('.deleteRec').show();
      }
    }
    $('#start_date_time').datetimepicker({
      timeFormat: 'hh:mm tt',
      dateFormat: 'MM dd, yy'
    });
    $('#end_date_time').datetimepicker({
      timeFormat: 'hh:mm tt',
      dateFormat: 'MM dd, yy'
    });
    if ($('#cnpc_link_type').val() === 'image') {
    alert("in");
      $('.form-item-files-cnpc-upload-image').show();
      $('.form-item-cnpc-buton-label').hide();
      $('.form-item-cnpc-text-label').hide();
    }
   else if ($('#cnpc_link_type').val() === 'text') {
      $('.form-item-files-cnpc-upload-image').hide();
      $('.form-item-cnpc-buton-label').hide();
      $('.form-item-cnpc-text-label').show();
     }
    else{
      $('.form-item-files-cnpc-upload-image').hide();
      $('.form-item-cnpc-buton-label').show();
      $('.form-item-cnpc-text-label').hide();
   
    }

    /**
     * Trimming fields and removing the HTML tags.
     */
    $('.cnpcaccount-add-form-group input[type="text"]').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('.cnpc-edit-form-group input[type="text"]').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('.cnpcaccount-add-channel-group input[type="text"]').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('.cnpc-edit-channel-group input[type="text"]').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('#cnpc_message').unbind('blur').bind('blur', function () {
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('#cnpc_account_number').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('#cnpc_account_guid').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });
    $('textarea[name="cnpc_params_textarea"]').unbind('blur').bind('blur', function () {
      removeHTMLTags($(this));
      const a = $(this).val().trim();
      $(this).val(a);
    });

    // Hide pledge TV errors.
    $('#cnpc_account_number').blur(() => {
      if ($('#cnpc_account_number').val() !== '') {
        verifyAccountNumber($('#cnpc_account_number').val());
      }
    });

    /**
     * Hide and show error information of Form Group Name CONNECT form.
     */
    $('#cnpc_form_group_name').blur(() => {
      let fs;
      if ($('#cnpc_form_group_name').val() !== '') {
        if ($('#cnpc_action').val() === 'add') {
          fs = verifyFormGroupName($('#cnpc_form_group_name').val());
          if (fs >= 1) {
            $('.cnpc_formexists_error').html('Form group name already exist');
            $('#savebtn').attr('disabled', 'disabled');
            $('#cnpc_form_group_name').focus();
          }
          else {
            $('.cnpc_formexists_error').html('');
            $('#savebtn').removeAttr('disabled');
          }
        }
        else if ($('#cnpc_action').val() === 'edit') {
          if ($('#hidden_fg_name').val() !== $('#cnpc_form_group_name').val()) {
            fs = verifyFormGroupName($('#cnpc_form_group_name').val());
            if (fs >= 1) {
              $('.cnpc_formexists_error').html('Form group name already exist');
              $('#finalsave').attr('disabled', 'disabled');
            }
            else {
              $('.cnpc_formexists_error').html('');
              $('#finalsave').removeAttr('disabled');
            }
          }
          else {
            $('.cnpc_formexists_error').html('');
            $('#finalsave').removeAttr('disabled');
          }
        }
      }
    });

    /**
     * Hide and show error information of channel group name in pledgeTV form.
     */
    $('#cnpc_channel_group_name').blur(() => {
      let fs;
      if ($('#cnpc_channel_group_name').val() !== '') {
        if ($('#cnpc_action').val() === 'add') {
          fs = verifyChannelGroupName($('#cnpc_channel_group_name').val());
          if (fs >= 1) {
            $('.cnpc_channelexists_error').html('Channel group name already exist');
            $('#chnlsavebtn').attr('disabled', 'disabled');
            $('#cnpc_channel_group_name').focus();
          }
          else {
            $('.cnpc_channelexists_error').html('');
            $('#chnlsavebtn').removeAttr('disabled');
          }
        }
        else if ($('#cnpc_action').val() === 'edit') {
          if ($('#hidden_fg_name').val() !== $('#cnpc_channel_group_name').val()) {
            fs = verifyChannelGroupName($('#cnpc_channel_group_name').val());
            if (fs >= 1) {
              $('.cnpc_channelexists_error').html('Channel group name already exist');
              $('#finalchnlsave').attr('disabled', 'disabled');
            }
            else {
              $('.cnpc_channelexists_error').html('');
              $('#finalchnlsave').removeAttr('disabled');
            }
          }
          else {
            $('.cnpc_channelexists_error').html('');
            $('#finalchnlsave').removeAttr('disabled');
          }
        }
      }
    });

    /**
     * To allows numbers in the account number input field of settings form.
     */
    $('#cnpc_account_number').keypress(function (e) {
      if (e.keyCode === 46 || e.keyCode === 8) {
        // let it happen, don't do anything.
      }
      else {
        // Ensure that it is a number and stop the keypress.
        if (e.keyCode < 48 || e.keyCode > 57) {
          return false;
        }
      }
    });
  });
  $('#cnpc_form_group_name').blur(() => {
    $('#cnpc_form_group_name').css('border', '1px solid #b8b8b8');
  });
  if ($('#cnpc_action').val() === 'edit') {
    $('.cnpc_settings_form').show();
  }
  else {
    $('.cnpc_settings_form').hide();
  }

  /**
   * Action to perform by clicking the save button in CONNECT form.
   *
   * It will display hidden div which contains campaigns information
   * like Campaign, Form, GUID, start date, end date.
   */
  $('#savebtn').click(() => {
    if (validateBasicForm()) {
      $('.cnpc_settings_form').show();
      getDataFromService($('#cnpc_counter').val());
      const tab = generateTable($('#cnpc_counter').val());
      $('tbody#dynarows').append(tab);

      $('.cnpcstartdate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.cnpcenddate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('#savebtn').hide();
      $('#resetbtn').hide();
      $('.cnpc_hide_show_buttons').show();
      $('#cnpc_counter').val(1);
    }
    return false;
  });
  // Add datepicker while edit form loads.
  /**
   * This event adds a new row with campign details.
   */
  $('#addrows').click((e) => {
    e.preventDefault();
    const rowIndex = parseInt($('#cnpc_counter').val());
    const downIndex = rowIndex - 1;
    if (cnpcCampListValidate() === false || cnpcFormListValidate() === false || cnpcStartDateValidate() === false || cnpcEndDateValidate() === false) {
      return false;
    }
    const rowCount = $('#custrows tr').length;
    if ($(`#enddate_${downIndex}`).val() === '') {
      $(`#enddate_${downIndex}`).next('p').remove();
      $(`#enddate_${downIndex}`).after('<p class="cnpctext-danger">You must select an end date before adding another form</p>');
      return false;
    }
    if ($(`#enddate_${downIndex}`).val() !== '') {
      $(`#enddate_${downIndex}`).next('p').remove();
    }
    if (validateDates(downIndex)) {
      const rows = generateTable(rowIndex);
      $('tbody#dynarows').append(rows);
      $('.cnpcstartdate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.cnpcenddate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      if (rowCount >= 2) {
        $('.deleteRec').show();
      }
      else {
        $('.deleteRec').hide();
      }
      getDataFromService(rowIndex);
      $('#cnpc_counter').val(rowIndex + 1);
    }
  });

  /**
   * This event fires when campaigns dropdown chnages in CONNECT form.
   *
   * Based on the value changes in campaign form, it will get the forms
   * related to that campaign and updates forms list under Form dropdown.
   */
  $(document).on('change', '.cnpc_camp_list', function () {
    const fi = $(this).data('rowindex');
    const campId = $(this).val();
    const accguid = $('#cnpc_accounts').val();
    const ss = accguid.split('||');
    const AccId = ss[0];
    const AccGuid = ss[1];
    const finalInfo = `${AccId}@${AccGuid}@${campId}`;
    if ($(this).val() !== '') {
      $(this).next('p').remove();
    }
    const basePath = $('#base_url_cnpc').val();
    const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/getactivityforms/${finalInfo}`;
    $.ajax({
      type: 'GET',
      url,
      success(res) {
        $(`#cnpc_form_list_${fi}`).html(res.data);
      }
    });
  });

  /**
   * This event fires on Form drop down in CONNECT Form.
   *
   * When the value changes in dropdown.
   */
  $(document).on('change', '.cnpc_form_list', function () {
    const indx = $(this).data('formindex');
    const PrevIndx = $(this).data('formindex') - 1;
    const PrevEndDt = new Date($(`#enddate_${PrevIndx}`).val());
    $(`#formguid_${$(this).data('formindex')}`).val($(this).val());
    if ($(this).data('formindex') === 0) {
      $(`#startdate_${$(this).data('formindex')}`).val($('#start_date_time').val());
    }
    else {
      $(`#startdate_${indx}`).datepicker('setDate', addOneDayToDate(PrevEndDt));
    }
    if ($(this).val() !== '') {
      $(this).next('p').remove();
    }
    if ($(`#startdate_${$(this).data('formindex')}`).val() !== '') {
      $(`#startdate_${$(this).data('formindex')}`).next().remove();
    }
  });

  /**
   * This event handles to delete Campaigns row.
   *
   * This delete button enbales when we are having more than 1 form.
   */
  $(document).on('click', '.deleteRec', function (e) {
    var recid;
    e.preventDefault();
    const cD = confirm('Are you sure want to delete?');
    if (cD === true) {
      const target = $(this).data('target');
      recid = $(this).data('recid');
      if ($('#custrows tr').length === 3) {
        $('#cnpc_counter').val($('#cnpc_counter').val() - $(this).data('index'));
        $(`#row_${$(this).data('index')}`).remove();
        $('.deleteRec').hide();
      }
      else {
        $(`#row_${$(this).data('index')}`).remove();
        $('#cnpc_counter').val($('#cnpc_counter').val() - $(this).data('index'));
        $('#cnpcchnl_counter').val($('#cnpcchnl_counter').val() - $(this).data('index'));
        if ($('#custchnlrows tr').length === 2) {
          $('.deleteRec').hide();
        }
      }
      if (target === 'edit') {
        recid = $(this).data('recid');
        const cact = $(this).data('caction');
        deleteFormDetails(recid, cact, $(this).attr('href'));
      }
    }
  });

  /**
   * Adding URL Parameters.
   *
   * This event will handles URL+ button, by clicking that button opens a popup
   * to add URL parameters.
   */
  $(document).on('click', '.cnpc-myBtn', function (e) {
    e.preventDefault();
    const pos = $(this).data('index');
    const plist = $(`#hidden_params_${pos}`).val();
    $('.cnpc-popup-success').remove();
    const newid = `params_${pos}`;
    $('.cnpc-form-group-params textarea').attr('id', newid);
    $('.cnpc-form-group-params textarea').attr('data-params-pos', pos);
    $('.cnpc-form-group-params textarea').val(plist);
    $('#addparams').attr('data-position', pos);
    $('#savecloseparams').attr('data-position', pos);
    $('#cnpc_myModal').show();
  });
  $('.cnpc-close').click(() => {
    $('.cnpc-form-group-params textarea').val('');
    $('#cnpc_myModal').hide();
  });
  $('.popupclosebtn').click((e) => {
    e.preventDefault();
    $('.cnpc-form-group-params textarea').val('');
    $('#cnpc_myModal').hide();
  });
  $('.popupsavebtn').click(function (e) {
    e.preventDefault();
    let ppos = '';
    $('.cnpc-popup-success').remove();
    ppos = $(this).attr('data-position');
    var listParams = $(`#params_${ppos}`).val();
    listParams = listParams.trim();
    if (listParams) {
      $(`#hidden_params_${ppos}`).val(listParams);
      $('.popupsavebtn').after('<span class="cnpc-popup-success">Parameters added</span>');
    }
  });

  /**
   * This event closes the popup window of URL parameters.
   */
  $('.popupsaveclosebtn').click(function (e) {
    e.preventDefault();
    let ppos = '';
    $('.cnpc-popup-success').remove();
    ppos = $(this).attr('data-position');
    var listParams = $(`#params_${ppos}`).val();
    listParams = listParams.trim();
    $(`#hidden_params_${ppos}`).val(listParams);
    $('.cnpc-form-group-params textarea').val('');
    $('#cnpc_myModal').hide();
  });

  $('textarea[name="cnpc_params_textarea"]').on('keypress', function (e) {
    removeHTMLTags($(this));
  });

  /**
   * To remove HTML tags from an input fields.
   *
   * @param {type} e
   *   The current element information.
   *
   * @return {undefined}
   *   Does not return any value.
   */
  function removeHTMLTags(e) {
    const val = $(`#${e.attr('id')}`).val();
    const regex = /(<([^>]+)>)/ig;
    const result = val.replace(regex, '');
    $(`#${e.attr('id')}`).val(result);
  }

  /**
   * To delete the Channel forms.
   *
   * @param {type} id
   *   ID number to delete the form.
   * @param {type} cact
   *   Holds form action that is add or edit.
   * @param {string} URL
   *   Holds current request url.
   *
   * @return {undefined}
   *   It does not returns any value.
   */
  function deleteFormDetails(id, cact, url) {
    $.ajax({
      type: 'GET',
      url: url,
      success() {},
      error() {}
    });
  }

  /**
   * To generate campaigns rows by clicking Add Form button.
   *
   * @param {type} rowIndex
   *   Table row index number for <tr>.
   *
   * @return {String}
   *   Return a set of form fields with in the table row.
   */
  function generateTable(rowIndex) {
    const bPath = $('#base_url_cnpc').val();
    let trow = '';
    trow += `<tr id='row_${rowIndex}'>`;
    trow += '<td>';
    trow += `<select  class='cnpc_camp_list' data-rowindex='${rowIndex}'  name='cnpc_camp_list[]' id='cnpc_camp_list_${rowIndex}'>`;
    trow += '<option value="">Select Campaign Name</option>';
    trow += `</select><img id='imgloader_${rowIndex}' src='${bPath}modules/clickandpledge_connect/images/loading.gif' height='25' width='25'>`;
    trow += '</td>';
    trow += '<td>';
    trow += `<select name='cnpc_form_list[]' data-formindex='${rowIndex}' class='cnpc_form_list' id='cnpc_form_list_${rowIndex}'>`;
    trow += '<option value="">Select Form Name</option>';
    trow += '</select>';
    trow += '</td>';
    trow += '<td>';
    trow += `<input type='text' name='formguid[]' id='formguid_${rowIndex}' disabled='disabled'>`;
    trow += '</td>';
    trow += '<td>';
    trow += `<input type='text' name='startdate[]' class='cnpcstartdate' id='startdate_${rowIndex}'>`;
    trow += '</td>';
    trow += '<td>';
    trow += `<input type='text' name='enddate[]' class='cnpcenddate' id='enddate_${rowIndex}'>`;
    trow += '</td>';
    trow += `<td class='cnpc_delurl_btns' id='row_${rowIndex}'><input type='hidden' id='hidden_params_${rowIndex}' name='hidden_params[]'>`;
    trow += `<div class='cnpc_url_btn'><a href='javascript:void(0)' class='cnpc-myBtn' data-index='${rowIndex}'>URL<b>+</b></a></div> &nbsp; <div class='cnpc_delete_btn'><a title='Delete' href='javascript:void(0)' id='deleteRec_${rowIndex}' data-target='add' class='deleteRec' data-index='${rowIndex}'><span class='bin_icon'></span></a></div>`;
    trow += '</td>';
    trow += '</tr>';
    return trow;
  }

  /**
   * To validate the campaigns dropdown in edit CONNECT form.
   *
   * @return {Boolean}
   *   Submits form without selecting campaign return false.
   */
  function cnpcCampListValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('select[name^="cnpc_camp_list_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a campaign</p>');
          stopper = false;
        }
      });
    }
    const inps1 = document.getElementsByName('cnpc_camp_list[]');
    for (let k = 0; k < inps1.length; k++) {
      if (inps1[k].value === '') {
        $(`#${inps1[k].id}`).next().remove();
        $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a campaign</p>');
        stopper = false;
        break;
      }
    }
    return stopper;
  }

  /**
   * To validate the form drop down.
   *
   * @return {Boolean}
   *   Returns false if we are not selecting form from list.
   */
  function cnpcFormListValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('select[name^="cnpc_form_list_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a form</p>');
          stopper = false;
        }
      });
    }
    const inps1 = document.getElementsByName('cnpc_form_list[]');
    for (let k = 0; k < inps1.length; k++) {
      if (inps1[k].value === '') {
        $(`#${inps1[k].id}`).next().remove();
        $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a form</p>');
        stopper = false;
        break;
      }
    }
    return stopper;
  }

  /**
   * To validate the start date of CONNECT form.
   *
   * @return {Boolean}
   *   Start date validation. if it is empty return false.
   */
  function cnpcStartDateValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('select[name^="startdate_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a start date</p>');
          stopper = false;
        }
      });
    }
    const inps1 = document.getElementsByName('startdate[]');
    for (let k = 0; k < inps1.length; k++) {
      if (inps1[k].value === '') {
        $(`#${inps1[k].id}`).next().remove();
        $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a start date</p>');
        stopper = false;
        break;
      }
    }
    return stopper;
  }

  /**
   * To validate the end date of CONNECT form.
   *
   * @return {Boolean}
   *   End date validation.
   */
  function cnpcEndDateValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('select[name^="enddate_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a end date</p>');
          stopper = false;
        }
      });
    }
    const inps1 = document.getElementsByName('enddate[]');
    for (let k = 0; k < inps1.length; k++) {
      if (k !== 0) {
        if (inps1[k].value === '') {
          if (k === (inps1.length - 1)) {
            stopper = true;
          }
          else {
            $(`#${inps1[k].id}`).next().remove();
            $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a end date</p>');
            stopper = false;
            break;
          }
        }
      }
    }
    return stopper;
  }

  /**
   * To get the list of Campaigns under an account.
   *
   * @param {type} ind
   *   The Account Guid.
   *
   * @return {undefined}
   *   Does not return any.
   */
  function getDataFromService(ind) {
    const accguid = $('#cnpc_accounts').val();
    const obj = accguid.split('||');
    const accid = obj[0];
    const guid = obj[1];
    const finalguid = `${accid}@${guid}`;
    const basePath = $('#base_url_cnpc').val();
    const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/getcampaigns/${finalguid}`;
    $.ajax({
      type: 'GET',
      url,
      beforeSend() {
        $(`#imgloader_${ind}`).show();
      },
      success(res) {
        $(`#cnpc_camp_list_${ind}`).html(res.data);
        $(`#imgloader_${ind}`).hide();
      },
      error() {
        alert('Unable to get campaigns');
      }
    });
  }

  /**
   * This event handles to save the CONNECT form information.
   *
   * Internall it calls vlidateBasicForm to validate form details like groupname
   * start&end dates,once click the save button it will opens Campaign forms.
   * Where we can click Add Forms button to add multiple forms to the group.
   */
  $('#finalsave').click(() => {
    if (!validateBasicForm()) {
      return false;
    }
    let stdt = $('#start_date_time').val();
    let eddt = $('#end_date_time').val();
    const greg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (stdt.match(greg)) {
      const gres = stdt.split(' ');
      const gres1 = gres[0].split('/');
      const gres2 = `${gres1[1]}/${gres1[0]}/${gres1[2]}`;
      stdt = `${gres2} ${gres[1]} ${gres[2]}`;
    }
    if (eddt !== '') {
      if (eddt.match(greg)) {
        const gcres = eddt.split(' ');
        const gcres1 = gcres[0].split('/');
        const gcres2 = `${gcres1[1]}/${gcres1[0]}/${gcres1[2]}`;
        eddt = `${gcres2} ${gcres[1]} ${gcres[2]}`;
      }
    }
    const mstdt = new Date(stdt);
    const meddt = new Date(eddt);
    let chksdt = $('#start_date_time').val();
    let chkedt = $('#end_date_time').val();
    const reg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (chksdt.match(reg)) {
      const res = chksdt.split(' ');
      const res1 = res[0].split('/');
      const res2 = `${res1[1]}/${res1[0]}/${res1[2]}`;
      chksdt = `${res2} ${res[1]} ${res[2]}`;
    }
    if (chkedt !== '') {
      if (chkedt.match(reg)) {
        const cres = chkedt.split(' ');
        const cres1 = cres[0].split('/');
        const cres2 = `${cres1[1]}/${cres1[0]}/${cres1[2]}`;
        chkedt = `${cres2} ${cres[1]} ${cres[2]}`;
      }
    }
    const mchksdt = new Date(chksdt);
    const mchkedt = new Date(chkedt);
    if ($('#start_date_time').val() === '') {
      $('#start_date_time').addClass('cnpcform-invalid');
      $('#start_date_time').focus();
      return false;
    }
    if (chksdt !== '' && chkedt !== '') {
      if (mchksdt.getTime() > mchkedt.getTime()) {
        $('#end_date_time').addClass('cnpcform-invalid');
        $('#end_date_time').focus();
        return false;
      }
    }
    // Display type validation.
    if ($('#cnpc_display_type').val() === '') {
      $('#cnpc_display_type').addClass('cnpcform-invalid');
      $('#cnpc_display_type').focus();
      return false;
    }
    if ($('#cnpc_display_type').val() === 'popup' && $('#cnpc_link_type').val() === '') {
      $('#cnpc_link_type').addClass('cnpcform-invalid');
      return false;
    }
    if ($('#cnpc_display_type').val() === 'popup' && ($('#cnpc_display_type').val() === 'text' || $('#cnpc_display_type').val() === 'button') && $('#cnpc_btn_lbl').val() === '') {
      $('#cnpc_btn_lbl').addClass('cnpcform-invalid');
      return false;
    }
    const form_data = new FormData();
    const formAction = $('#cnpc_action').val();
    const camp = cnpcCampListValidate();
    const form = cnpcFormListValidate();
    const sdate = cnpcStartDateValidate();
    const edate = cnpcEndDateValidate();
    let nofrms = $('#cnpc_counter').val();
    nofrms -= 1;
    for (let j = 0; j <= nofrms; j++) {
      let strtdt1 = $(`#startdate_${j}`).val();
      let chkedt1 = $(`#enddate_${j}`).val();
      const ereg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
      if (strtdt1.match(ereg)) {
        const eres = strtdt1.split(' ');
        const eres1 = eres[0].split('/');
        const eres2 = `${eres1[1]}/${eres1[0]}/${eres1[2]}`;
        strtdt1 = `${eres2} ${eres[1]} ${eres[2]}`;
      }
      if (chkedt1 !== '') {
        if (chkedt1.match(ereg)) {
          const ecres = chkedt1.split(' ');
          const ecres1 = ecres[0].split('/');
          const ecres2 = `${ecres1[1]}/${ecres1[0]}/${ecres1[2]}`;
          chkedt1 = `${ecres2} ${ecres[1]} ${ecres[2]}`;
        }
      }
      const mchksdt1 = new Date(strtdt1);
      const mchkedt1 = new Date(chkedt1);
      if (strtdt1 !== '' && chkedt1 === '') {
        if (mchksdt1.getTime() < mstdt.getTime()) {
          $(`#startdate_${j}`).next().remove();
          $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
          $(`#startdate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() > meddt.getTime()) {
          $(`#startdate_${j}`).next().remove();
          $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be less than or equal to group end date</p>');
          $(`#startdate_${j}`).focus();
          return false;
        }
      }
      if (strtdt1 !== '' && chkedt1 !== '') {
        if (mchkedt1.getTime() > meddt.getTime()) {
          $(`#enddate_${j}`).next().remove();
          $(`#enddate_${j}`).after('<p class="cnpc-danger">Form end date should be less than or equal to group end date</p>');
          $(`#enddate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() < mstdt.getTime()) {
          $(`#startdate_${j}`).next().remove();
          $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
          $(`#startdate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() > mchkedt1.getTime()) {
          $(`#enddate_${j}`).next().remove();
          $(`#enddate_${j}`).after('<p class="cnpc-danger">End date should be greater than or equal to start date</p>');
          $(`#enddate_${j}`).focus();
          return false;
        }
      }
    }
    if (camp && form && sdate && edate) {
      const cnpcfgn = $('#cnpc_form_group_name').val();
      const cnpcacc = $('#cnpc_accounts').val();
      const cnpcsdt = $('#start_date_time').val();
      const cnpcedt = $('#end_date_time').val();
      const cnpcdtype = $('#cnpc_display_type').val();
      form_data.append('cnpc_action', formAction);
      form_data.append('cnpc_group_name', cnpcfgn);
      form_data.append('cnpc_account', cnpcacc);
      form_data.append('cnpc_start_datetime', cnpcsdt);
      form_data.append('cnpc_end_datetime', cnpcedt);
      form_data.append('cnpc_display_type', cnpcdtype);
      if (cnpcdtype === 'popup') {
        const cnpcltype = $('#cnpc_link_type').val();
        form_data.append('link_type', cnpcltype);
        if (cnpcltype === 'image') {
          const {
            files
          } = document.getElementById('cnpc_upload_image');
          if (files.length !== 0) {
            if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpeg') {
              form_data.append('file', files[0]);
            }
            else {
              alert('Please select a valid image(.png, .jpg)');
              $('#cnpc_upload_image').focus();
              return false;
            }
          }
          else if ($('#cnpc_action').val() === 'edit') {
            if ($('#cnpc_upload_image_hidden').val() !== '') {
              const hiddenImg = $('#cnpc_upload_image_hidden').val();
              form_data.append('file', hiddenImg);
            }
            else {
              $('#cnpc_upload_image').css('border', '1px solid red');
              $('#cnpc_upload_image').focus();
              return false;
            }
          }
          else {
            alert('Please select an image');
            $('#cnpc_upload_image').focus();
            return false;
          }
        }
       else if (cnpcltype === 'text') {
          const txtlbl = $('#cnpc_txt_lbl').val();
          const linktype = $('#cnpc_link_type').val();
          form_data.append('link_type', linktype);
          if (txtlbl !== '') {
            form_data.append('text_label', txtlbl);
          }
          else {
            alert('Please enter Text label');
            $('#cnpc_txt_lbl').focus();
            return false;
          }
        }
        else {
          const btnlbl = $('#cnpc_btn_lbl').val();
          const linktype = $('#cnpc_link_type').val();
          form_data.append('link_type', linktype);
          if (btnlbl !== '') {
            form_data.append('button_label', btnlbl);
          }
          else {
            alert('Please enter button label');
            $('#cnpc_btn_lbl').focus();
            return false;
          }
        }
      }
      const cmpcmsg = $('#cnpc_message').val();
      const status = $('#cnpc_status').val();
      form_data.append('cnpc_message', cmpcmsg);
      form_data.append('cnpc_status', status);
      const url = $('#cnpc_form_save_url').val();
      // Collect the data from groups.
      let camp_list;
      let form_list;
      let form_list_titles;
      let formguid;
      let startdate;
      let enddate;
      let paramsList;
      if (formAction === 'add') {
        camp_list = getValues('cnpc_camp_list[]');
        form_list = getValues('cnpc_form_list[]');
        form_list_titles = getTitles('cnpc_form_list[]');
        formguid = getValues('formguid[]');
        startdate = getValues('startdate[]');
        enddate = getValues('enddate[]');
        paramsList = getValues('hidden_params[]');
      }
      else {
        camp_list = getFormSelectValues('cnpc_camp_list');
        form_list = getFormSelectValues('cnpc_form_list');
        form_list_titles = getSelectTitles('cnpc_form_list');
        formguid = getFormValues('formguid');
        startdate = getFormValues('startdate');
        enddate = getFormValues('enddate');
        const cnpc_edit_id = $('#cnpc_edit_id').val();
        paramsList = getFormValues('hidden_params');
        form_data.append('cnpc_edit_id', cnpc_edit_id);
      }
      form_data.append('cnpc_camp_list', camp_list);
      form_data.append('cnpc_form_list', form_list);
      form_data.append('formguid', formguid);
      form_data.append('startdate', startdate);
      form_data.append('enddate', enddate);
      form_data.append('form_list_titles', form_list_titles);
      form_data.append('params_list', paramsList);
      $.ajax({
        url,
        data: form_data,
        cache: false,
        contentType: false,
        method: 'POST',
        processData: false,
        dataType: 'json',
        success(res) {
          if (res.data === 'success') {
            if (formAction === 'edit') {
              window.location = '../cnp_form';
            }
            else {
              window.location = 'cnp_form';
            }
          }
          return false;
        }
      });
    }
    return false;
  });

  /**
   * Read all the values of a campaign.
   *
   * @param {type} name
   *   Name of the text box.
   *
   * @return {String}
   *   Returns the value of a textbox.
   */
  function getValues(name) {
    let fdata = '';
    const inps = document.getElementsByName(name);
    for (let i = 0; i < inps.length; i++) {
      const inp = inps[i];
      fdata += `${inp.value}###`;
    }
    return fdata;
  }

  /**
   * Get the form values.
   *
   * @param {type} name
   *   Name of the input box.
   *
   * @return {String}
   *   Returns value of the input box based on name.
   */
  function getFormValues(name) {
    let fvdata = '';
    $(`input[name^="${name}"]`).each(function () {
      fvdata += `${$(`#${$(this).attr('id')}`).val()}###`;
    });
    return fvdata;
  }

  /**
   * Get the form selected values.
   *
   * @param {type} name
   *   Value of the textbox.
   *
   * @return {String}
   *   Returns value of the textbox.
   */
  function getFormSelectValues(name) {
    let fsdata = '';
    $(`select[name^="${name}"]`).each(function () {
      fsdata += `${$(`#${$(this).attr('id')}`).val()}###`;
    });
    return fsdata;
  }

  /**
   * Extract titles from dropdown.
   *
   * To get all the option titles from a dropdowns which are selected and
   * concatinate them into a variable to return.
   *
   * @param {type} name
   *   Get value of the option.
   *
   * @return {String}
   *   Returns the selected option of a dropdown.
   */
  function getSelectTitles(name) {
    let ftitles = '';
    $(`select[name^="${name}"]`).each(function () {
      ftitles += `${$(`#${$(this).attr('id')} option:selected`).text()}###`;
    });
    return ftitles;
  }

  /**
   * To get the name of the option selected in a dropdown.
   *
   * @param {type} name
   *   Option value from the drop down.
   *
   * @return {String}
   *   Returns option text based on option value.
   */
  function getTitles(name) {
    let fdata = '';
    const inps = document.getElementsByName(name);
    for (let i = 0; i < inps.length; i++) {
      const inp = inps[i];
      fdata += `${$(`#${inp.id} option:selected`).text()}###`;
    }
    return fdata;
  }

  /**
   * The below function adds 1 day to the date passed as parameter.
   *
   * @param {type} dateStr
   *   Current Date.
   *
   * @return {String}
   *   Returns the Month based on the date provided.
   */
  function addOneDayToDate(dateStr) {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    const result = new Date(new Date(dateStr).setMinutes(new Date(dateStr).getMinutes() + 1));
    const d = result;
    const dt = monthNames[d.getMonth()];
    let day = d.getDate();
    if (day <= 9) {
      day = `0${day}`;
    }
    return `${dt} ${day}, ${d.getFullYear()} ${formatAMPM(dateStr)}`;
  }

  /**
   * To calculate AM or PM based on the date passed.
   *
   * @param {type} date
   *   Selected date from start and end dates to append AM or PM based on the
   *   date provided.
   *
   * @return {String}
   *   Returns Am or PM.
   */
  function formatAMPM(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes() + 1;
    const ampm = hours >= 12 ? 'pm' : 'am';
    hours %= 12;
    hours = hours || 12; // The hour '0' should be '12'.
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    const strTime = `${hours}:${minutes} ${ampm}`;
    return strTime;
  }

  if ($('#cnpc_display_type').val() === 'popup') {
    $('.overlay_wrapper').show();
  }
  else {
    $('.overlay_wrapper').hide();
  }

  /**
   * This event handles Display type in the CONNECT forms dropdown.
   *
   * Default display type is inline.
   */
  $('#cnpc_display_type').change(function () {
    if ($(this).val() === 'popup') {
      $('.overlay_wrapper').show();
    }
    else {
      $('.overlay_wrapper').hide();
    }
  });

  /**
   * Display default button type.
   *
   * This event handles Link type in the CONNECT forms dropdown
   * default display type is button.
   */
  $('#cnpc_link_type').change(function () {
    if ($(this).val() === 'image') {
      $('.form-item-files-cnpc-upload-image').show();
      $('.form-item-cnpc-buton-label').hide();
      $('.form-item-cnpc-text-label').hide();
    }
   else if ($(this).val() === 'text'){
      $('.form-item-files-cnpc-upload-image').hide();
      $('.form-item-cnpc-buton-label').hide();
      $('.form-item-cnpc-text-label').show(); 
    }
    else {
      $('.form-item-files-cnpc-upload-image').hide();
      $('.form-item-cnpc-buton-label').show();
      $('.form-item-cnpc-text-label').hide(); 
    }
  });

  /**
   * This method validates the start date and End date of CONNECT form group.
   *
   * We can not select previous date of current date
   * start date and end date should be greater than the current date always
   * end date should not below the start date.
   *
   * @param {type} j
   *   Index of start and end dates.
   *
   * @return {Boolean}
   *   Does not return empty.
   */
  function validateDates(j) {
    let status = true;
    let strtdt1 = $(`#startdate_${j}`).val();
    let chkedt1 = $(`#enddate_${j}`).val();
    let stdt = $('#start_date_time').val();
    let eddt = $('#end_date_time').val();
    const greg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (stdt.match(greg)) {
      const gres = stdt.split(' ');
      const gres1 = gres[0].split('/');
      const gres2 = `${gres1[1]}/${gres1[0]}/${gres1[2]}`;
      stdt = `${gres2} ${gres[1]} ${gres[2]}`;
    }
    if (eddt !== '') {
      if (eddt.match(greg)) {
        const gcres = eddt.split(' ');
        const gcres1 = gcres[0].split('/');
        const gcres2 = `${gcres1[1]}/${gcres1[0]}/${gcres1[2]}`;
        eddt = `${gcres2} ${gcres[1]} ${gcres[2]}`;
      }
    }
    const mstdt = new Date(stdt);
    const meddt = new Date(eddt);
    const ereg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (strtdt1.match(ereg)) {
      const eres = strtdt1.split(' ');
      const eres1 = eres[0].split('/');
      const eres2 = `${eres1[1]}/${eres1[0]}/${eres1[2]}`;
      strtdt1 = `${eres2} ${eres[1]} ${eres[2]}`;
    }
    if (chkedt1 !== '') {
      if (chkedt1.match(ereg)) {
        const ecres = chkedt1.split(' ');
        const ecres1 = ecres[0].split('/');
        const ecres2 = `${ecres1[1]}/${ecres1[0]}/${ecres1[2]}`;
        chkedt1 = `${ecres2} ${ecres[1]} ${ecres[2]}`;
      }
    }
    const mchksdt1 = new Date(strtdt1);
    const mchkedt1 = new Date(chkedt1);
    if (strtdt1 !== '' && chkedt1 === '') {
      if (mchksdt1.getTime() < mstdt.getTime()) {
        $(`#startdate_${j}`).next().remove();
        $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
        $(`#startdate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() > meddt.getTime()) {
        $(`#startdate_${j}`).next().remove();
        $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be less than or equal to group end date</p>');
        $(`#startdate_${j}`).focus();
        status = false;
      }
    }
    if (strtdt1 !== '' && chkedt1 !== '') {
      if (mchkedt1.getTime() > meddt.getTime()) {
        $(`#enddate_${j}`).next().remove();
        $(`#enddate_${j}`).after('<p class="cnpc-danger">Form end date should be less than or equal to group end date</p>');
        $(`#enddate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() < mstdt.getTime()) {
        $(`#startdate_${j}`).next().remove();
        $(`#startdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
        $(`#startdate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() > mchkedt1.getTime()) {
        $(`#enddate_${j}`).next().remove();
        $(`#enddate_${j}`).after('<p class="cnpc-danger">End date should be greater than or equal to start date</p>');
        $(`#enddate_${j}`).focus();
        status = false;
      }
    }
    return status;
  }

  /**
   * The below event handles CONNECT form status as active or inactive.
   *
   * Active forms will be dispay on pages, inactive forms will not display in pages.
   */
  var fstatus;
  $('.formstatus').click(function (event) {
    event.preventDefault();
    if ($(this).html() === 'active') {
      fstatus = 'inactive';
      $(this).html(fstatus);
    }
    else if ($(this).html() === 'inactive') {
      fstatus = 'active';
      $(this).html(fstatus);
    }
    const url = $(this).attr('href');
    $.ajax({
      url,
      type: 'GET',
      success() {
      },
      error() {
        alert('Unable to update form status');
      }
    });
  });

  /**
   * The below event handles channel form status as active or inactive.
   *
   * Active forms will be dispay on pages, inactive form will not display in pages.
   */
  $('.ptvformstatus').click(function (event) {
    event.preventDefault();
    const elemID = $(this).attr('id');
    if ($(this).html() === 'active') {
      fstatus = 'inactive';
      $(this).html(fstatus);
    }
    else if ($(this).html() === 'inactive') {
      fstatus = 'active';
      $(this).html(fstatus);
    }
    const url = $(this).attr('href');
    $.ajax({
      url,
      type: 'GET',
      success(res) {
        if (res.data === 'success') {
          $(`#${elemID}`).html(fstatus);
          $(`#${elemID}`).attr('data-fstatus', fstatus);
        }
      }
    });
  });

  /**
   * Reads the cookie information.
   *
   * @param {type} name
   *   Name of the cookie.
   *
   * @return {unresolved}
   *   Returns the value of a cookie.
   */
  function readCookie(name) {
    const nameEQ = `${name}=`;
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }
  if (readCookie('settingsdel')) {
    $('.cnpc_error_msg').show();
    $('.cnpc_error_msg').focus();
    $('.cnpc_error_msg').addClass('cnpc_success');
    $('.cnpc_error_msg').html(decodeURI(readCookie('settingsdel')));
  }

  /**
   * This event helps to get updated data from the CONNECT platform.
   */
  $('.refreshAccounts').click(function (e) {
    e.preventDefault();
    const check = confirm('I am refreshing the account information, please wait');
    if (check === true) {
      const settguid = $(this).data('settguid');
      const settaccid = $(this).data('settaccid');
      const recid = $(this).data('settid');
      const url = $(this).attr('href');
      $.ajax({
        url,
        type: 'POST',
        data: {
          sguid: settguid,
          saccid: settaccid,
          srecid: recid
        },
        success(res) {
          if (res.data === 'success') {
            alert('All account information has been updated.  Please click OK to continue.');
            window.location = 'cnp_formssettings';
          }
          else {
            alert('All account information has been updated.  Please click OK to continue.');
            window.location = 'cnp_formssettings';
          }
        },
        error() {
          alert('Unable to refresh accounts');
        }
      });
    }
  });

  /**
   * This change event handles on Accounts drop down in both.
   *
   * Forms(CONNECT and pledgeTV).When we chnage the account it will clear all
   * entered data, because it load the fresh data of the selected account.
   */
  $('#cnpc_accounts').change(() => {
    // Form Acocunts change.
    if ($('#cnpc_counter').val() >= 1) {
      var clr1 = confirm('Changing the account will clear all entered data. Are you sure?');
      if (clr1 === true) {
        $('#cnpc_counter').val(0);
        $('#custrows').find('tr:gt(0)').remove();
        getDataFromService($('#cnpc_counter').val());
        const tab = generateTable($('#cnpc_counter').val());
        $('tbody#dynarows').append(tab);

        $('.cnpcstartdate').each(function () {
          $(this).datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy'
          });
        });
        $('.cnpcenddate').each(function () {
          $(this).datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy'
          });
        });
        $('#cnpc_counter').val(1);
      }
    }
    // PledgeTV Accounts Change.
    let tab1;
    if ($('#cnpcchnl_counter').val() >= 1) {
      var clr2 = confirm('Changing the account will clear all entered data. Are you sure?');
      if (clr2 === true) {
        $('#cnpcchnl_counter').val(0);
        $('#custchnlrows').find('tr:gt(0)').remove();
        getDataChannelService($('#cnpcchnl_counter').val());
        tab1 = generateChannelTable($('#cnpcchnl_counter').val());
        $('tbody#dynachnlrows').append(tab1);
        $('.cnpcstartdate').each(function () {
          $(this).datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy'
          });
        });
        $('.cnpcenddate').each(function () {
          $(this).datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy'
          });
        });
        $('#cnpcchnl_counter').val(1);
      }
    }
  });

  /**
   * PledgeTV Forms JS Starts Here.
   */

  /**
   * This event handles to create a new channel under a channel form group.
   */
  $('#addchannelrows').click((e) => {
    e.preventDefault();
    if (cnpcChnlListValidate() === false || cnpcchnlStartDateValidate() === false || cnpcchnlEndDateValidate() === false) {
      return false;
    }
    const crowCount = $('#custchnlrows tr').length;
    const crowIndex = parseInt($('#cnpcchnl_counter').val());
    const cdownIndex = crowIndex - 1;
    if ($(`#chnlenddate_${cdownIndex}`).val() === '') {
      $(`#chnlenddate_${cdownIndex}`).next('p').remove();
      $(`#chnlenddate_${cdownIndex}`).after('<p class="cnpctext-danger">End date is required</p>');
      return false;
    }
    if ($(`#chnlenddate_${cdownIndex}`).val() !== '') {
      $(`#chnlenddate_${cdownIndex}`).next('p').remove();
    }
    if (validateChannelDates(cdownIndex)) {
      const crows = generateChannelTable(crowIndex);
      $('tbody#dynachnlrows').append(crows);
      $('.cnpcstartdate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      $('.cnpcenddate').each(function () {
        $(this).datetimepicker({
          timeFormat: 'hh:mm tt',
          dateFormat: 'MM dd, yy'
        });
      });
      if (crowCount >= 2) {
        $('.deleteRec').show();
      }
      else {
        $('.deleteRec').hide();
      }
      getDataChannelService(crowIndex);
      $('#cnpcchnl_counter').val(crowIndex + 1);
    }
  });

  /**
   * This method validates the start date and End date of channel form group.
   *
   * We can not select previous date of current date
   * start date and end date should be greater than the current date always
   * end date should not below the start date.
   *
   * @param {number} j
   *   Holds index number of start and end date.
   *
   * @return {Boolean}
   *   On successfull validation it will returns true otherwise false.
   */
  function validateChannelDates(j) {
    let status = true;
    let strtdt1 = $(`#chnlstartdate_${j}`).val();
    let chkedt1 = $(`#chnlenddate_${j}`).val();
    let stdt = $('#start_date_time').val();
    let eddt = $('#end_date_time').val();
    const greg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (stdt.match(greg)) {
      const gres = stdt.split(' ');
      const gres1 = gres[0].split('/');
      const gres2 = `${gres1[1]}/${gres1[0]}/${gres1[2]}`;
      stdt = `${gres2} ${gres[1]} ${gres[2]}`;
    }
    if (eddt !== '') {
      if (eddt.match(greg)) {
        const gcres = eddt.split(' ');
        const gcres1 = gcres[0].split('/');
        const gcres2 = `${gcres1[1]}/${gcres1[0]}/${gcres1[2]}`;
        eddt = `${gcres2} ${gcres[1]} ${gcres[2]}`;
      }
    }
    const mstdt = new Date(stdt);
    const meddt = new Date(eddt);
    const ereg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (strtdt1.match(ereg)) {
      const eres = strtdt1.split(' ');
      const eres1 = eres[0].split('/');
      const eres2 = `${eres1[1]}/${eres1[0]}/${eres1[2]}`;
      strtdt1 = `${eres2} ${eres[1]} ${eres[2]}`;
    }
    if (chkedt1 !== '') {
      if (chkedt1.match(ereg)) {
        const ecres = chkedt1.split(' ');
        const ecres1 = ecres[0].split('/');
        const ecres2 = `${ecres1[1]}/${ecres1[0]}/${ecres1[2]}`;
        chkedt1 = `${ecres2} ${ecres[1]} ${ecres[2]}`;
      }
    }
    const mchksdt1 = new Date(strtdt1);
    const mchkedt1 = new Date(chkedt1);
    if (strtdt1 !== '' && chkedt1 === '') {
      if (mchksdt1.getTime() < mstdt.getTime()) {
        $(`#chnlstartdate_${j}`).next().remove();
        $(`#chnlstartdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
        $(`#chnlstartdate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() > meddt.getTime()) {
        $(`#chnlstartdate_${j}`).next().remove();
        $(`#chnlstartdate_${j}`).after('<p class="cnpc-danger">Form start date should be less than or equal to group end date</p>');
        $(`#chnlstartdate_${j}`).focus();
        status = false;
      }
    }
    if (strtdt1 !== '' && chkedt1 !== '') {
      if (mchkedt1.getTime() > meddt.getTime()) {
        $(`#chnlenddate_${j}`).next().remove();
        $(`#chnlenddate_${j}`).after('<p class="cnpc-danger">Form end date should be less than or equal to group end date</p>');
        $(`#chnlenddate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() < mstdt.getTime()) {
        $(`#chnlstartdate_${j}`).next().remove();
        $(`#chnlstartdate_${j}`).after('<p class="cnpc-danger">Form start date should be greater or equal to group start date</p>');
        $(`#chnlstartdate_${j}`).focus();
        status = false;
      }
      if (mchksdt1.getTime() > mchkedt1.getTime()) {
        $(`#chnlenddate_${j}`).next().remove();
        $(`#chnlenddate_${j}`).after('<p class="cnpc-danger">End date should be greater than or equal to start date</p>');
        $(`#chnlenddate_${j}`).focus();
        status = false;
      }
    }
    return status;
  }

  /**
   * Validate the each and every channels drop down under a channel group.
   *
   * @return {Boolean}
   *   With out selecting the channel if form submitted, it will be valiadted.
   */
  function cnpcChnlListValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('select[name^="cnpc_chnl_list_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a channel</p>');
          stopper = false;
        }
        else {
          $(`#${$(this).attr('id')}`).next().remove();
        }
      });
    }
    const inps1 = document.getElementsByName('cnpc_chnl_list[]');
    for (let k = 0; k < inps1.length; k++) {
      if (inps1[k].value === '') {
        $(`#${inps1[k].id}`).next().remove();
        $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a channel</p>');
        stopper = false;
        break;
      }
      else {
        $(`#${inps1[k].id}`).next().remove();
      }
    }
    return stopper;
  }

  /**
   * Method for validating start date of each and every form in channel group.
   *
   * @return {Boolean}
   *   Channel start date validation, if it is not selected validated.
   */
  function cnpcchnlStartDateValidate() {
    let stopper = true;
    if ($('#cnpc_action').val() === 'edit') {
      $('input[name^="chnlstartdate_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a start date</p>');
          stopper = false;
        }
        else {
          $(`#${$(this).attr('id')}`).next().remove();
        }
      });
    }
    const inps1 = document.getElementsByName('chnlstartdate[]');
    for (let k = 0; k < inps1.length; k++) {
      if (inps1[k].value === '') {
        $(`#${inps1[k].id}`).next().remove();
        $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a start date</p>');
        stopper = false;
        break;
      }
      else {
        $(`#${inps1[k].id}`).next().remove();
      }
    }
    return stopper;
  }

  /**
   * Method for validating end date of each and every form in channel group.
   *
   * @return {Boolean}
   *   Validate channel end date is with in the range, returns true otherwise
   *   returns false.
   */
  function cnpcchnlEndDateValidate() {
    let stopper = true;
  
  const inps1 = document.getElementsByName('chnlenddate[]');
    if ($('#cnpc_action').val() === 'edit') {
    if(inps1.length >=1){
      $('input[name^="chnlenddate_"]').each(function () {
        if ($(this).val() === '') {
          $(`#${$(this).attr('id')}`).next().remove();
          $(`#${$(this).attr('id')}`).after('<p class="cnpctext-danger">Please select a end date</p>');
          stopper = false;
        }
        else {
          $(`#${$(this).attr('id')}`).next().remove();
        }
      });
    }
    }
 
    for (let k = 0; k < inps1.length; k++) {
      if (k !== 0) {
        if (inps1[k].value === '') {
          if (k === (inps1.length - 1)) {
            $(`#${inps1[k].id}`).next().remove();
            stopper = true;
          }
          else {
            $(`#${inps1[k].id}`).next().remove();
            $(`#${inps1[k].id}`).after('<p class="cnpctext-danger">Please select a end date</p>');
            stopper = false;
            break;
          }
        }
      }
    }
    return stopper;
  }

  /**
   * Get all the channels to display in the channels dropdown.
   *
   * @param {number} ind
   *   The Account GUID
   */
  function getDataChannelService(ind) {
    const accguid = $('#cnpc_accounts').val();
    const obj = accguid.split('||');
    const accid = obj[0];
    const guid = obj[1];
    const finalguid = `${accid}@${guid}`;
    const basePath = $('#base_url_cnpc').val();
    const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/getchannels/${finalguid}`;
    $.ajax({
      type: 'GET',
      url,
      beforeSend() {
        $(`#imgloader_${ind}`).show();
      },
      success(res) {
        $(`#cnpc_chnl_list_${ind}`).html(res.data);
        $(`#imgloader_${ind}`).hide();
      },
      error() {
        alert('Unable to get the channels');
      }
    });
  }

  /**
   * Handles basic validations of Add new Channel form.
   *
   * It will validates fileds like date and time validations,data validation
   * after it will save the Channel form data into database table.
   */
  $('#finalchnlsave').click(() => {
    if ($('#cnpc_channel_group_name').val() === '') {
      $('#cnpc_channel_group_name').css('border', '1px solid red');
      $('#cnpc_channel_group_name').focus();
      return false;
    }

    $('#cnpc_channel_group_name').css('border', '1px');

    if ($('#start_date_time').val() === '') {
      $('#start_date_time').css('border', '1px solid red');
      $('#start_date_time').focus();
      return false;
    }
    const stdt = $('#start_date_time').val();
    const eddt = $('#end_date_time').val();
    const mstdt = new Date(stdt);
    const meddt = new Date(eddt);
    let chksdt = $('#start_date_time').val();
    let chkedt = $('#end_date_time').val();
    const reg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (chksdt.match(reg)) {
      const res = chksdt.split(' ');
      const res1 = res[0].split('/');
      const res2 = `${res1[1]}/${res1[0]}/${res1[2]}`;
      chksdt = `${res2} ${res[1]} ${res[2]}`;
    }
    if (chkedt !== '') {
      if (chkedt.match(reg)) {
        const cres = chkedt.split(' ');
        const cres1 = cres[0].split('/');
        const cres2 = `${cres1[1]}/${cres1[0]}/${cres1[2]}`;
        chkedt = `${cres2} ${cres[1]} ${cres[2]}`;
      }
    }
    const mchksdt = new Date(chksdt);
    const mchkedt = new Date(chkedt);
    if ($('#start_date_time').val() === '') {
      $('#start_date_time').addClass('cnpcform-invalid');
      $('#start_date_time').focus();
      return false;
    }
    if (chksdt !== '' && chkedt !== '') {
      if (mchksdt.getTime() > mchkedt.getTime()) {
        $('#end_date_time').addClass('cnpcform-invalid');
        $('#end_date_time').focus();
        return false;
      }
    }
     const form_data = new FormData();
    const camp = cnpcChnlListValidate();
    const sdate = cnpcchnlStartDateValidate();
    const edate = cnpcchnlEndDateValidate();
    let nofrms = $('#cnpcchnl_counter').val();
    nofrms -= 1;
    for (let j = 0; j <= nofrms; j++) {
      let strtdt1 = $(`#chnlstartdate_${j}`).val();
      let chkedt1 = $(`#chnlenddate_${j}`).val();
      const ereg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
      if (strtdt1.match(ereg)) {
        const eres = strtdt1.split(' ');
        const eres1 = eres[0].split('/');
        const eres2 = `${eres1[1]}/${eres1[0]}/${eres1[2]}`;
        strtdt1 = `${eres2} ${eres[1]} ${eres[2]}`;
      }
      if (chkedt1 !== '') {
        if (chkedt1.match(ereg)) {
          const ecres = chkedt1.split(' ');
          const ecres1 = ecres[0].split('/');
          const ecres2 = `${ecres1[1]}/${ecres1[0]}/${ecres1[2]}`;
          chkedt1 = `${ecres2} ${ecres[1]} ${ecres[2]}`;
        }
      }
      const mchksdt1 = new Date(strtdt1);
      const mchkedt1 = new Date(chkedt1);
      if (strtdt1 !== '' && chkedt1 === '') {
        if (mchksdt1.getTime() < mstdt.getTime()) {
          $(`#chnlstartdate_${j}`).next().remove();
          $(`#chnlstartdate_${j}`).after('<p class="cnpctext-danger">Channel start date should be greater or equal to group start date</p>');
          $(`#chnlstartdate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() > meddt.getTime()) {
          $(`#chnlstartdate_${j}`).next().remove();
          $(`#chnlstartdate_${j}`).after('<p class="cnpctext-danger">Channel start date should be less than or equal to group end date</p>');
          $(`#chnlstartdate_${j}`).focus();
          return false;
        }
      }
      if (strtdt1 !== '' && chkedt1 !== '') {
        if (mchkedt1.getTime() > meddt.getTime()) {
          $(`#chnlenddate_${j}`).next().remove();
          $(`#chnlenddate_${j}`).after('<p class="cnpctext-danger">Channel end date should be less than or equal to group end date</p>');
          $(`#chnlenddate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() < mstdt.getTime()) {
          $(`#chnlstartdate_${j}`).next().remove();
          $(`#chnlstartdate_${j}`).after('<p class="cnpctext-danger">Channel start date should be greater or equal to group start date</p>');
          $(`#chnlstartdate_${j}`).focus();
          return false;
        }
        if (mchksdt1.getTime() > mchkedt1.getTime()) {
          $(`#chnlenddate_${j}`).next().remove();
          $(`#chnlenddate_${j}`).after('<p class="cnpctext-danger">End date should be greater than or equal to start date</p>');
          $(`#chnlenddate_${j}`).focus();
          return false;
        }
      }
    }
    if (camp && sdate && edate) {
     
      const cnpcfgn = $('#cnpc_channel_group_name').val();
      const cnpcsdt = $('#start_date_time').val();
      const cnpcedt = $('#end_date_time').val();
      const cmpcmsg = $('#cnpc_message').val();
      const status = $('#cnpc_status').val();
      const formAction = $('#cnpc_action').val();
      const cnpcacc = $('#cnpc_accounts').val();
      form_data.append('cnpc_action', formAction);
      form_data.append('cnpc_group_name', cnpcfgn);
      form_data.append('cnpc_start_datetime', cnpcsdt);
      form_data.append('cnpc_end_datetime', cnpcedt);
      form_data.append('cnpc_account', cnpcacc);
      form_data.append('cnpc_message', cmpcmsg);
      form_data.append('cnpc_status', status);
      const url = $('#cnpc_channel_save_url').val();
      let camp_list;
      let startdate;
      let enddate;
      if (formAction === 'add') {
        camp_list = getValues('cnpc_chnl_list[]');
        startdate = getValues('chnlstartdate[]');
        enddate = getValues('chnlenddate[]');
      }
      else {
        camp_list = getFormSelectValues('cnpc_chnl_list');
        startdate = getFormValues('chnlstartdate');
        enddate = getFormValues('chnlenddate');
        const cnpc_edit_id = $('#cnpc_edit_id').val();
        form_data.append('cnpc_edit_id', cnpc_edit_id);
      }
      form_data.append('cnpc_chnl_list', camp_list);
      form_data.append('chnlstartdate', startdate);
      form_data.append('chnlenddate', enddate);
      $.ajax({
        url,
        data: form_data,
        cache: false,
        contentType: false,
        method: 'POST',
        processData: false,
        dataType: 'json',
        success(res) {
          if (res.data === 'success') {
            if (formAction === 'edit') {
              window.location = '../CnPpledgeTVChannels';
            }
            else {
              window.location = 'CnPpledgeTVChannels';
            }
          }
          return false;
        },
        error() {
          alert('Unable to insert channel data');
        }
      });
    }
    return false;
  });

  /**
   * Genarates the forms table.
   *
   * This function calls when user clicks Add Channel button, it will add more
   * no.of channels under a campaign.
   *
   * @param {string} rowIndex
   *   Contains index number to generate table row.
   *
   * @return {String}
   *   Returns a string which contains a row with form fields.
   */
  function generateChannelTable(rowIndex) {
    const bPath = $('#base_url_cnpc').val();
    let trow = '';
    trow += `<tr id="row_${rowIndex}">`;
    trow += '<td>';
    trow += `<select  class="cnpc_chnl_list" data-rowindex="${rowIndex}"  name="cnpc_chnl_list[]" id="cnpc_chnl_list_${rowIndex}">`;
    trow += '<option value="">Select Channel</option>';
    trow += `</select><img id="imgloader_${rowIndex}" src="${bPath}modules/clickandpledge_connect/images/loading.gif" height="25" width="25">`;
    trow += '</td>';
    trow += '<td>';
    trow += `<input type="text" name="chnlstartdate[]" class="cnpcstartdate" id="chnlstartdate_${rowIndex}">`;
    trow += '</td>';
    trow += '<td>';
    trow += `<input type="text" name="chnlenddate[]" class="cnpcenddate" id="chnlenddate_${rowIndex}">`;
    trow += '</td>';
    trow += `<td id="row_${rowIndex}">`;
    trow += `<a href="javascript:void(0)" id="deleteRec_${rowIndex}" data-target="add" class="deleteRec" data-index="${rowIndex}"><span class="bin_icon"></span></a>`;
    trow += '</td>';
    trow += '</tr>';
    return trow;
  }

  /**
   * Add New Channel form validation.
   *
   * If this form is valid,then it allows to add the campaign forms.
   */
  $('#chnlsavebtn').click(() => {
    let fs;
    if ($('#cnpc_action').val() === 'add') {
      fs = verifyChannelGroupName($('#cnpc_channel_group_name').val());
      if (fs >= 1) {
        $('.cnpc_channelexists_error').html('Channel group name already exist');
        $('#chnlsavebtn').attr('disabled', 'disabled');
        $('#cnpc_channel_group_name').focus();
      }
      else {
        $('.cnpc_channelexists_error').html('');
        $('#chnlsavebtn').removeAttr('disabled');
      }
    }
    else if ($('#cnpc_action').val() === 'edit') {
      if ($('#hidden_fg_name').val() !== $('#cnpc_channel_group_name').val()) {
        fs = verifyFormGroupName($('#cnpc_channel_group_name').val());
        if (fs >= 1) {
          $('.cnpc_channelexists_error').html('Channel group name already exist');
          $('#finalchnlsave').attr('disabled', 'disabled');
        }
        else {
          $('.cnpc_channelexists_error').html('');
          $('#finalchnlsave').removeAttr('disabled');
        }
      }
      else {
        $('.cnpc_channelexists_error').html('');
        $('#finalchnlsave').removeAttr('disabled');
      }
    }
    let stdt = $('#start_date_time').val();
    let eddt = $('#end_date_time').val();
    const greg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (stdt.match(greg)) {
      const gres = stdt.split(' ');
      const gres1 = gres[0].split('/');
      const gres2 = `${gres1[1]}/${gres1[0]}/${gres1[2]}`;
      stdt = `${gres2} ${gres[1]} ${gres[2]}`;
    }
    if (eddt !== '') {
      if (eddt.match(greg)) {
        const gcres = eddt.split(' ');
        const gcres1 = gcres[0].split('/');
        const gcres2 = `${gcres1[1]}/${gcres1[0]}/${gcres1[2]}`;
        eddt = `${gcres2} ${gcres[1]} ${gcres[2]}`;
      }
    }
    const mstdt = new Date(stdt);
    const meddt = new Date(eddt);
    if ($('#cnpc_channel_group_name').val() === '') {
      $('#cnpc_channel_group_name').css('border', '1px solid red');
      $('#cnpc_channel_group_name').focus();
      return false;
    }
    $('#cnpc_channel_group_name').css('border', '1px');
    if ($('#start_date_time').val() === '') {
      $('#start_date_time').css('border', '1px solid red');
      $('#start_date_time').focus();
      return false;
    }
    if (mstdt.getTime() > meddt.getTime()) {
      $('#start_date_time').css('border', '1px solid red');
      $('#start_date_time').focus();
      return false;
    }
    $('.cnpc_settings_form').show();
    getDataChannelService($('#cnpcchnl_counter').val());
    const tab = generateChannelTable($('#cnpcchnl_counter').val());
    $('tbody#dynachnlrows').append(tab);
    $('.cnpcstartdate').each(function () {
      $(this).datetimepicker({
        timeFormat: 'hh:mm tt',
        dateFormat: 'MM dd, yy'
      });
    });
    $('.cnpcenddate').each(function () {
      $(this).datetimepicker({
        timeFormat: 'hh:mm tt',
        dateFormat: 'MM dd, yy'
      });
    });
    $('#chnlsavebtn').hide();
    $('#resetbtn').hide();
    $('.cnpc_hide_show_buttons').show();
    $('#cnpcchnl_counter').val(1);
    return false;
  });

  /**
   * Displaying  tooltip regarding short code usage.
   *
   * Handing with a tooltip in connect forms view table, when we kept mouse on
   * question  mark, opens a tooltip which guides how to use short code.
   */
  $('.cnpc_question').mouseover(() => {
    $('.cnpc_shortcode_tooltip').show();
  });
  $('.cnpc_question').mouseout(() => {
    $('.cnpc_shortcode_tooltip').hide();
  });
  $('.cnpc_shortcode_tooltip').mouseover(function () {
    $(this).show();
  });
  $('.cnpc_shortcode_tooltip').mouseout(function () {
    $(this).hide();
  });

  /**
   * Validates the Connect Form for both Add and Edit.
   *
   * @return {Boolean}
   *   Returns true if the form validated successfully otherwire return false
   */
  function validateBasicForm() {
    let status = true;
    let fs;
    if ($('#cnpc_action').val() === 'add') {
      fs = verifyFormGroupName($('#cnpc_form_group_name').val());
      if (fs >= 1) {
        $('.cnpc_formexists_error').html('Form group name already exist');
        $('#savebtn').attr('disabled', 'disabled');
        $('#cnpc_form_group_name').focus();
      }
      else {
        $('.cnpc_formexists_error').html('');
        $('#savebtn').removeAttr('disabled');
      }
    }
    else if ($('#cnpc_action').val() === 'edit') {
      if ($('#hidden_fg_name').val() !== $('#cnpc_form_group_name').val()) {
        fs = verifyFormGroupName($('#cnpc_form_group_name').val());
        if (fs >= 1) {
          $('.cnpc_formexists_error').html('Form group name already exist');
          $('#finalsave').attr('disabled', 'disabled');
        }
        else {
          $('.cnpc_formexists_error').html('');
          $('#finalsave').removeAttr('disabled');
        }
      }
      else {
        $('.cnpc_formexists_error').html('');
        $('#finalsave').removeAttr('disabled');
      }
    }
    if ($('#cnpc_form_group_name').val() === '') {
      $('#cnpc_form_group_name').css('border', '1px solid red');
      $('#cnpc_form_group_name').focus();
      status = false;
    }
    let stdt = $('#start_date_time').val();
    let eddt = $('#end_date_time').val();
    const greg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (stdt.match(greg)) {
      const gres = stdt.split(' ');
      const gres1 = gres[0].split('/');
      const gres2 = `${gres1[1]}/${gres1[0]}/${gres1[2]}`;
      stdt = `${gres2} ${gres[1]} ${gres[2]}`;
    }
    if (eddt !== '') {
      if (eddt.match(greg)) {
        const gcres = eddt.split(' ');
        const gcres1 = gcres[0].split('/');
        const gcres2 = `${gcres1[1]}/${gcres1[0]}/${gcres1[2]}`;
        eddt = `${gcres2} ${gcres[1]} ${gcres[2]}`;
      }
    }
    let chksdt = $('#start_date_time').val();
    let chkedt = $('#end_date_time').val();
    const reg = /(0?[1-9]|1[0-2])[^\w\d\r\n:](0?[1-9]|[12]\d|30|31)[^\w\d\r\n:](\d{4}|)/;
    if (chksdt.match(reg)) {
      const res = chksdt.split(' ');
      const res1 = res[0].split('/');
      const res2 = `${res1[1]}/${res1[0]}/${res1[2]}`;
      chksdt = `${res2} ${res[1]} ${res[2]}`;
    }
    if (chkedt !== '') {
      if (chkedt.match(reg)) {
        const cres = chkedt.split(' ');
        const cres1 = cres[0].split('/');
        const cres2 = `${cres1[1]}/${cres1[0]}/${cres1[2]}`;
        chkedt = `${cres2} ${cres[1]} ${cres[2]}`;
      }
    }
    const mchksdt = new Date(chksdt);
    const mchkedt = new Date(chkedt);
    if ($('#start_date_time').val() === '') {
      $('#start_date_time').addClass('cnpcform-invalid');
      $('#start_date_time').focus();
      status = false;
    }
    if (chksdt !== '' && chkedt !== '') {
      if (mchksdt.getTime() > mchkedt.getTime()) {
        $('#end_date_time').addClass('cnpcform-invalid');
        $('#end_date_time').focus();
        status = false;
      }
    }
    // Display type validation.
    if ($('#cnpc_display_type').val() === '') {
      $('#cnpc_display_type').addClass('cnpcform-invalid');
      $('#cnpc_display_type').focus();
      status = false;
    }
    if ($('#cnpc_display_type').val() === 'popup' && $('#cnpc_link_type').val() === '') {
      $('#cnpc_link_type').addClass('cnpcform-invalid');
      status = false;
    }
  if ($('#cnpc_display_type').val() === 'popup' &&  $('#cnpc_link_type').val() === 'button' && $('#cnpc_btn_lbl').val() === '') {
      $('#cnpc_btn_lbl').addClass('cnpcform-invalid');
      status = false;
    }
    if ($('#cnpc_display_type').val() === 'popup' && $('#cnpc_link_type').val() === 'text'  && $('#cnpc_txt_lbl').val() === '') {
      $('#cnpc_txt_lbl').addClass('cnpcform-invalid');
      status = false;
    }
    const cnpcdtype = $('#cnpc_display_type').val();
    if (cnpcdtype === 'popup') {
      const cnpcltype = $('#cnpc_link_type').val();
      if (cnpcltype === 'image') {
        const {
          files
        } = document.getElementById('cnpc_upload_image');
        if (files.length !== 0) {
          if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpeg') {
            $('#cnpc_upload_image').css('border', '1px solid rgb(184, 184, 184)');
          }
          else {
            $('#cnpc_upload_image').css('border', '1px solid red');
            $('#cnpc_upload_image').focus();
            status = false;
          }
        }
        else {
          if ($('#cnpc_action').val() !== 'edit') {
            $('#cnpc_upload_image').css('border', '1px solid red');
            $('#cnpc_upload_image').focus();
            status = false;
          }
        }
      }
      else  if (cnpcltype === 'button') {
         const btnlbl = $('#cnpc_btn_lbl').val();
      
        if (btnlbl !== '') {
          $('#cnpc_btn_lbl').css('border', '1px solid rgb(184, 184, 184)');
        }
        else {
          $('#cnpc_btn_lbl').css('border', '1px solid red');
          $('#cnpc_btn_lbl').focus();
          status = false;
        }
       
      }
    else
    {
     
         const txtlbl = $('#cnpc_txt_lbl').val();
     
        if (txtlbl !== '') {
          $('#cnpc_txt_lbl').css('border', '1px solid rgb(184, 184, 184)');
        }
        else {
          $('#cnpc_txt_lbl').css('border', '1px solid red');
          $('#cnpc_txt_lbl').focus();
          status = false;
        }
    }
    }
    return status;
  }

  /**
   * Verify the account number already exists in table or not.
   *
   * @param {number} number
   *   Account Number for checking duplicates.
   */
  function verifyAccountNumber(number) {
    const basePath = $('#base_url_cnpc').val();
    const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/verifyaccnumber/${number}`;
    $.ajax({
      type: 'GET',
      url,
      success(res) {
        if (res >= 1) {
          $('.cnpc_exists_error').html('Account already exist.');
          $('input[value="Verify"]').attr('disabled', 'disabled');
          $('#cnpc_account_number').focus();
        }
        else {
          $('.cnpc_exists_error').html('');
          $('input[value="Verify"]').removeAttr('disabled');
        }
      },
      error() {
        alert('Unable to verify account number');
      }
    });
  }

  /**
   * To verify the from group name for avoiding duplicate group names.
   *
   * @param {string} fname
   *   Contains channel group name
   *
   * @return {res}
   *   Returns status true if channel exists otherwire returns false.
   */
  function verifyFormGroupName(fname) {
    if (fname !== '') {
      let fStatus = null;
      const basePath = $('#base_url_cnpc').val();
      const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/verifyformgroupname/${fname}`;
      $.ajax({
        type: 'GET',
        url,
        cache: false,
        dataType: 'html',
        async: false,
        success(res) {
          fStatus = res;
        }
      });
      return fStatus;
    }
  }

  /**
   * To verify the channel group name for avoiding duplicate channel names.
   *
   * @param {string} fname
   * @return {res}
   */

  function verifyChannelGroupName(fname) {
    if (fname !== '') {
      let fStatus = null;
      const basePath = $('#base_url_cnpc').val();
      const url = `${location.protocol}//${location.host}${basePath}admin/clickandpledge_connect/verifychannelgroupname/${fname}`;
      $.ajax({
        type: 'GET',
        url,
        cache: false,
        dataType: 'html',
        async: false,
        success(res) {
          fStatus = res;
        }
      });
      return fStatus;
    }
  }
}(jQuery, Drupal));
