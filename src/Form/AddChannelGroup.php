<?php

namespace Drupal\clickandpledge_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Uses to create pledgeTV channel forms.
 */
class AddChannelGroup extends FormBase {

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnpcaccount_add_channel_group';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#theme'] = 'cnp_add_channel_group';
    $displayForm = $this->displayChannelgroupsForm($form, $form_state);
    return $displayForm;
  }

  /**
   * Creates a form to store channel groups information.
   *
   * @param mixed $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displayChannelgroupsForm($form, $form_state) {
    date_default_timezone_set(date_default_timezone_get());
    $form['cnpc_channel_group_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel Group Name*'),
      '#description' => $this->t('Please enter the channel group name<br><span class="cnpc_channelexists_error"></span>'),
      '#default_value' => '',
      '#attributes' => ['id' => 'cnpc_channel_group_name'],
    ];
    $data = $this->getAccountRecords();
    $options = [];
    foreach ($data as $rec) {
      $options[$rec->cnpstngs_AccountNumber . "||" . $rec->cnpstngs_guid . "||" . $rec->cnpstngs_ID] = $rec->cnpstngs_frndlyname . ' ( ' . $rec->cnpstngs_AccountNumber . ' )';
    }
    $form['cnpc_channel_save_url'] = [
      '#type' => 'hidden',
      '#default_value' => Url::fromRoute('cnpconnect.cnpc_save_channel_data', [])->toString(),
      '#attributes' => ['id' => 'cnpc_channel_save_url'],
    ];
    $form['base_url_cnpc'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnpc'],
    ];
    $form['cnpc_action'] = [
      '#type' => 'hidden',
      '#default_value' => 'add',
      '#attributes' => ['id' => 'cnpc_action'],
    ];
    $form['cnpc_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Account(s)*'),
      '#options' => $options,
      '#attributes' => ['id' => 'cnpc_accounts'],
    ];
    $form['cnpc_start_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Date/Time* @dateC', ['@dateC' => '[TimeZone: ' . date_default_timezone_get() . ']']),
      '#attributes' => ['id' => 'start_date_time'],
      '#default_value' => date('F d, Y h:i a'),
    ];
    $form['cnpc_end_date_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End Date/Time'),
      '#attributes' => ['id' => 'end_date_time'],
      '#default_value' => '',
    ];
    $form['cnpc_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Valid Channel Message'),
      '#attributes' => ['id' => 'cnpc_message'],
      '#default_value' => $this->t('Sorry! This channel is expired'),
    ];
    $form['cnpc_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        'active' => $this->t('Active'),
        'inactive' => $this->t('Inactive'),
      ],
      '#attributes' => ['id' => 'cnpc_status'],
    ];
    $form['cnpcchnl_counter'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'cnpcchnl_counter'],
      '#default_value' => 0,
    ];
    // Hidden form Starts here, once click the save button it will display the
    // hidden form to add channels to channel group.
    $form['save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'chnlsavebtn'],
    ];
    $form['final_save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#attributes' => ['id' => 'finalchnlsave'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Form data is saving by using ajax calls.
  }

  /**
   * Get all the accounts and displayed on accounts drop down in the form.
   *
   * @return array
   *   Returns an array of account details.
   */
  public function getAccountRecords() {
    $sql = 'SELECT cnpstngs_ID,cnpstngs_frndlyname,cnpstngs_AccountNumber,cnpstngs_guid,cnpstngs_status,cnpstngs_Date_Created,cnpstngs_Date_Modified FROM {dp_cnpcsettingsdtl}';
    $query = $this->connection->query($sql);
    return $query->fetchAll();
  }

}
