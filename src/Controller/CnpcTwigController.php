<?php

namespace Drupal\clickandpledge_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SimpleXMLElement;

/**
 * The Controller class to display and render the forms via twig templates.
 */
class CnpcTwigController extends ControllerBase {

  /**
   * Class Contsnats.
   */
  const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
  const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";

  /**
   * Declaare SOAP version.
   */
  const CFCNP_SOAP_VER = "SOAP_1_1";

  /**
   * CFCNP_SERVICE_URL.
   *
   * Service URL to authenticate and to get forms and campaigns from click
   * and pledge CONNECT platform.
   */
  const CFCNP_SERVICE_URL = "https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl";

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * To get the path.
   *
   * @var mixed
   */
  protected $path;

  /**
   * The module handler service.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * The Class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database Connection.
   * @param \Drupal\Core\Path\CurrentPathStack $pathStack
   *   Represents the current path for the current request.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(Connection $connection, CurrentPathStack $pathStack, ModuleHandlerInterface $module_handler) {
    $this->connection = $connection;
    $this->path = $pathStack;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('path.current'), $container->get('module_handler')
    );
  }

  /**
   * To display help information of the module.
   *
   * @return array
   *   Returns an array to render twig template.
   */
  public function cnpcFromHelp() {
    return [
      '#theme' => 'cnp_form_help',
    ];
  }

  /**
   * To display channels under a channel group.
   *
   * @param mixed $variable
   *   The channel group id.
   *
   * @return array
   *   Returns a render array with template and data associated with template.
   */
  public function cnpcViewChannels($variable) {
    $sql = "SELECT cnpchannel_id,cnpchannel_cnpchannelgrp_ID,cnpchannel_channelName,cnpchannel_channelStartDate,"
      . "cnpchannel_channelEndDate,cnpchannel_channelStatus,cnpchannel_DateCreated,"
      . "cnpchannel_DateModified FROM {dp_cnpc_channeldtl} where cnpchannel_cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $variable]);
    $allForms = $query->fetchAll();
    return [
      '#theme' => 'cnpc_viewchannels',
      '#channels' => $allForms,
    ];
  }

  /**
   * To view all the forms under a form group.
   *
   * @param mixed $variable
   *   The form group id.
   *
   * @return array
   *   Returns an array with template and data which is associated with the
   *   template
   */
  public function cnpcViewForms($variable) {
    $sql = "SELECT cnpform_id,cnpform_cnpform_ID,cnpform_CampaignName,cnpform_FormName,cnpform_GUID,"
      . "cnpform_FormStartDate,cnpform_FormEndDate,cnpform_urlparameters,cnpform_FormStatus,"
      . "cnpform_DateCreated,cnpform_DateModified FROM {dp_cnpc_formsdtl} where "
      . "cnpform_cnpform_ID=:fid";
    $query = $this->connection->query($sql, [':fid' => $variable]);
    $allForms = $query->fetchAll();
    return [
      '#theme' => 'cnpc_viewforms',
      '#allforms' => $allForms,
    ];
  }

  /**
   * To display all the form groups and its information in a table.
   *
   * @return array
   *   Returns an array to display data in a template.
   */
  public function getAllFormGroups() {
    return [
      '#theme' => 'cnpc_all_form_groups',
      '#allgroups' => $this->getAllFormGroupRecords(),
    ];
  }

  /**
   * To display all pledgeTV channel groups in table.
   *
   * @return array
   *   Returns an array with all channel groups to display in template.
   */
  public function getAllPledgetvChannels() {
    return [
      '#theme' => 'cnpc_all_pledgetv_groups',
      '#allchannelgroups' => $this->getAllChannelRecords(),
    ];
  }

  /**
   * To display settings form.
   *
   * @return array
   *   Returns an array with form and data.
   */
  public function cnpcSettingsForm() {
    $form = $this->formBuilder()->getForm('Drupal\clickandpledge_connect\Form\CnPAccountSettings');
    return [
      '#theme' => 'cnpc_account_settings_form',
      '#sform' => $form,
      '#settingsdata' => $this->getSettingsRecords(),
    ];
  }

  /**
   * To display edit form for form group editing.
   *
   * @param mixed $variable
   *   The channel group id.
   *
   * @return mixed
   *   Returns an with form and data to be displayed in edit form.
   */
  public function cnpcEditFormGroup($variable) {
    $form = $this->formBuilder()->getForm('Drupal\clickandpledge_connect\Form\EditForms');
    $formDetails = $this->getFormDetails($variable);
    $getInfo = $this->getFormGroupInfo($variable);
    $campId = $getInfo->cnpform_AccountNumber . "@" . $getInfo->cnpform_guid . "@" . $getInfo->cnpform_cnpstngs_ID;
    $campOpt = $this->getAllCampaignsList($campId);
    $formListData = [];
    for ($f = 0; $f < count($formDetails); $f++) {
      $fl = $getInfo->cnpform_AccountNumber . "@" . $getInfo->cnpform_guid . "@" . $formDetails[$f]->cnpform_CampaignName;
      $formListData[] = $this->getActivityFormsList($fl);
    }
    return [
      '#theme' => 'cnpc_edit_form_group',
      '#eform' => $form,
      '#editformdata' => 'Welcome',
      '#allcampaigns' => $campOpt,
      '#formList' => $formListData,
      '#formdetails' => $formDetails,
    ];
  }

  /**
   * To get all the campaigns to display in the forms.
   *
   * @param string $variable
   *   The variable contains Account GUID and Account Id separated by @ symbol.
   *
   * @return array
   *   Returns an array contains all campaigns under an account.
   */
  public function getAllCampaignsList($variable) {
    $str         = explode("@", $variable);
    $connect     = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client      = new \SoapClient(self::CFCNP_SERVICE_URL, $connect);
    $accountguid = $str[1];
    $accountid   = $str[0];
    $xmlr        = new SimpleXMLElement("<GetActiveCampaignList2></GetActiveCampaignList2>");
    $xmlr->addChild('accountId', $accountid);
    $xmlr->addChild('AccountGUID', $accountguid);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $response = $client->GetActiveCampaignList2($xmlr);
    $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;
    if (count($responsearr) == 1) {
      $camrtrnval[$responsearr->alias] = $responsearr->name;
    }
    else {
      for ($inc = 0; $inc < count($responsearr); $inc++) {
        $camrtrnval[$responsearr[$inc]->alias] = $responsearr[$inc]->name;
      }
    }
    natcasesort($camrtrnval);
    array_unshift($camrtrnval, $this->t('Select Campaign Name'));
    return $camrtrnval;
  }

  /**
   * To get the activity forms list of a campaign .
   *
   * @param string $variable
   *   The Variable contains AccountId, Account GUID and Campaign id.
   *
   * @return string
   *   Returns an array contains all activity forms under a campaign.
   */
  public function getActivityFormsList($variable) {
    $str              = explode("@", $variable);
    $connect          = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client           = new \SoapClient(self::CFCNP_SERVICE_URL, $connect);
    $cnpaccountID     = $str[0];
    $cnpaccountguidID = $str[1];
    $cnpcampaignId    = $str[2];
    $xmlr             = new SimpleXMLElement("<GetActiveFormList2></GetActiveFormList2>");
    $xmlr->addChild('accountId', $cnpaccountID);
    $xmlr->addChild('AccountGUID', $cnpaccountguidID);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $xmlr->addChild('campaignAlias', $cnpcampaignId);
    $frmresponse    = $client->GetActiveFormList2($xmlr);
    $frmresponsearr = $frmresponse->GetActiveFormList2Result->form;
   if( !is_array($frmresponsearr)){
    //if (count($frmresponsearr) == 1) {
      $rtrnval[$frmresponsearr->formGUID] = $frmresponsearr->formName;
    }
    else {
      for ($finc = 0; $finc < count($frmresponsearr); $finc++) {
        $rtrnval[$frmresponsearr[$finc]->formGUID] = $frmresponsearr[$finc]->formName;
      }
    }
    natcasesort($rtrnval);
    array_unshift($rtrnval, $this->t('Select Form Name'));
    return $rtrnval;
  }

  /**
   * To get the forms information under a form form group.
   *
   * @param int $id
   *   Form group id.
   *
   * @return array
   *   Returns the information about a form under a campaign.
   */
  public function getFormGroupInfo($id) {
    $sql = "SELECT cnpform_GID,cnpform_groupname,cnpform_cnpstngs_ID,cnpform_AccountNumber,cnpform_guid,"
            . "cnpform_type,cnpform_ptype,cnpform_text,cnpform_img,cnpform_shortcode,cnpform_custommsg,"
            . "cnpform_Form_StartDate,cnpform_Form_EndDate,cnpform_status,cnpform_Date_Created,"
            . "cnpform_Date_Modified FROM {dp_cnpc_forminfo} where cnpform_GID=:gid";
    $query = $this->connection->query($sql, [':gid' => $id]);
    return $query->fetch();
  }

  /**
   * Get form details to display the forms.
   *
   * @param int $id
   *   Form id.
   *
   * @return array
   *   Returns an array contains individual form details.
   */
  public function getFormDetails($id) {
    $sql = "SELECT cnpform_id,cnpform_cnpform_ID,cnpform_CampaignName,cnpform_FormName,cnpform_GUID,"
            . "cnpform_FormStartDate,cnpform_FormEndDate,cnpform_urlparameters,cnpform_FormStatus,"
            . "cnpform_DateCreated,cnpform_DateModified FROM {dp_cnpc_formsdtl} where cnpform_cnpform_ID=:fid";
    $query = $this->connection->query($sql, [':fid' => $id]);
    return $query->fetchAll();
  }

  /**
   * Get all the CONNECT form records.
   *
   * @return array
   *   Returns an array contains form details.
   */
  public function getAllFormGroupRecords() {
    $gdata = [];
    $sql = "SELECT cnpform_GID,cnpform_groupname,cnpform_cnpstngs_ID,cnpform_AccountNumber,"
      . "cnpform_guid,cnpform_type,cnpform_ptype,cnpform_text,cnpform_img,cnpform_shortcode,"
      . "cnpform_custommsg,cnpform_Form_StartDate,cnpform_Form_EndDate,cnpform_status,"
      . "cnpform_Date_Created,cnpform_Date_Modified FROM {dp_cnpc_forminfo}";
    $query = $this->connection->query($sql);
    foreach ($query->fetchAll() as $groups) {
      $innerData = [];
      if ($groups->cnpform_Form_EndDate == '0000-00-00 00:00:00') {
        $enddate = '';
      }
      else {
        $enddate = date('F d, Y h:i:s a', strtotime($groups->cnpform_Form_EndDate));
      }
      if ($this->checkFormsExpiry($groups->cnpform_Form_StartDate, $enddate) === "no") {
        $innerData['expiry_status'] = "no";
      }
      else {
        $innerData['expiry_status'] = "yes";
      }

      $innerData['noofforms'] = count($this->getNoofForms($groups->cnpform_GID));
      foreach ($groups as $groupkey => $groupvalue) {
        $innerData[$groupkey] = $groupvalue;
      }
      $gdata[] = $innerData;
    }
    return $gdata;
  }

  /**
   * Get pledgeTV channels from db to display into a table.
   *
   * @return array
   *   Returns channel and its settings information as an array.
   */
  public function getAllChannelRecords() {
    $gdata = [];
    $sql = 'SELECT * FROM {dp_cnpc_channelgrp} INNER JOIN {dp_cnpcsettingsdtl} ON cnpchannelgrp_cnpstngs_ID=cnpstngs_ID';
    $query = $this->connection->query($sql);
    foreach ($query->fetchAll() as $groups) {
      $innerData = [];
      if ($groups->cnpchannelgrp_channel_EndDate == '0000-00-00 00:00:00') {
        $enddate = '';
      }
      else {
        $enddate = date('F d, Y h:i:s a', strtotime($groups->cnpchannelgrp_channel_EndDate));
      }
      if ($this->checkChannelFormsExpiry($groups->cnpchannelgrp_channel_StartDate, $enddate) === "no") {
        $innerData['expiry_status'] = "no";
      }
      else {
        $innerData['expiry_status'] = "yes";
      }

      $innerData['noofforms'] = count($this->getChannelNoofChannels($groups->cnpchannelgrp_ID));
      foreach ($groups as $groupkey => $groupvalue) {
        $innerData[$groupkey] = $groupvalue;
      }
      $gdata[] = $innerData;
    }
    return $gdata;
  }

  /**
   * Get total no.of forms to display the count in the table view.
   *
   * @param int $id
   *   The form group id.
   *
   * @return array
   *   Get the total no.of forms.
   */
  public function getNoofForms($id) {
    $sql = "SELECT cnpform_id,cnpform_cnpform_ID,cnpform_CampaignName,cnpform_FormName,cnpform_GUID,"
      . "cnpform_FormStartDate,cnpform_FormEndDate,cnpform_urlparameters,cnpform_FormStatus,"
      . "cnpform_DateCreated,cnpform_DateModified FROM {dp_cnpc_formsdtl} where cnpform_cnpform_ID=:formID";
    $query = $this->connection->query($sql, [':formID' => $id]);
    return $query->fetchAll();
  }

  /**
   * To get the total no of channels under a form group.
   *
   * @param int $id
   *   The Channel group ID.
   *
   * @return array
   *   Returns all the channels in an array.
   */
  public function getChannelNoofChannels($id) {
    $sql = "SELECT cnpchannel_id,cnpchannel_cnpchannelgrp_ID,cnpchannel_channelName,cnpchannel_channelStartDate,"
      . "cnpchannel_channelEndDate,cnpchannel_channelStatus,cnpchannel_DateCreated,"
      . "cnpchannel_DateModified FROM {dp_cnpc_channeldtl} where cnpchannel_cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $id]);
    return $query->fetchAll();
  }

  /**
   * To display channel edit form.
   *
   * @param mixed $variable
   *   The channel group id.
   *
   * @return mixed
   *   Returns an array with form and data to be displayed in edit form.
   */
  public function cnpcEditChannelGroup($variable) {
    $form = $this->formBuilder()->getForm('Drupal\clickandpledge_connect\Form\EditChannels');
    $getInfo = $this->getChannelInfo($variable);
    $campId = $getInfo->cnpstngs_AccountNumber . "@" . $getInfo->cnpstngs_guid . "@" . $getInfo->cnpstngs_ID;
    $campOpt = $this->getAllChannelsList($campId);
    return [
      '#theme' => 'cnpc_edit_channel_group',
      '#ecform' => $form,
      '#channellist' => $campOpt,
      '#formdetails' => $this->getChannelDetails($variable),
    ];
  }

  /**
   * Get all the channels list from SOAP service.
   *
   * @param string $variable
   *   Contains AccountId and GUID separated with @ symbol.
   *
   * @return string
   *   Returns an array with channel name and channel URL id.
   */
  public function getAllChannelsList($variable) {
    $str         = explode("@", $variable);
    $connect     = [
      'soap_version' => self::CFCNP_SOAP_VER,
      'trace' => 1,
      'exceptions' => 0,
    ];
    $module_path = $this->moduleHandler->getModule('clickandpledge_connect')->getPath() . '/Auth2.wsdl';
    $client      = new \SoapClient($module_path, $connect);
    $accountguid = $str[1];
    $accountid   = $str[0];
    $xmlr        = new SimpleXMLElement('<GetPledgeTVChannelList></GetPledgeTVChannelList>');
    $xmlr->addChild('accountId', $accountid);
    $xmlr->addChild('AccountGUID', $accountguid);
    $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
    $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
    $response = $client->GetPledgeTVChannelList($xmlr);
    $responsearr = $response->GetPledgeTVChannelListResult->PledgeTVChannel;
 if( !is_array($responsearr)){
   // if (count($responsearr) == 1) {
      $camrtrnval[$responsearr->ChannelURLID] = $responsearr->ChannelName;
    }
    else {
      for ($inc = 0; $inc < count($responsearr); $inc++) {
        $camrtrnval[$responsearr[$inc]->ChannelURLID] = $responsearr[$inc]->ChannelName;
      }
    }
    natcasesort($camrtrnval);
    array_unshift($camrtrnval, $this->t('Select Channel Name'));
    return $camrtrnval;
  }

  /**
   * Get the channel information based channel group settings id.
   *
   * @param int $id
   *   Channel group settings id.
   *
   * @return array
   *   Returns an array of records which contains campaign forms forms.
   */
  public function getChannelInfo($id) {
    $sql = "SELECT * FROM {dp_cnpc_channelgrp} inner join {dp_cnpcsettingsdtl} on cnpchannelgrp_cnpstngs_ID = cnpstngs_ID where cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $id]);
    return $query->fetch();
  }

  /**
   * Get the channel details based on channel group id.
   *
   * @param int $id
   *   Channel group Id.
   *
   * @return array
   *   Returns channel details array.
   */
  public function getChannelDetails($id) {
    $sql = "SELECT cnpchannel_id,cnpchannel_cnpchannelgrp_ID,cnpchannel_channelName,cnpchannel_channelStartDate,"
      . "cnpchannel_channelEndDate,cnpchannel_channelStatus,cnpchannel_DateCreated,"
      . "cnpchannel_DateModified FROM {dp_cnpc_channeldtl} where cnpchannel_cnpchannelgrp_ID=:grpid";
    $query = $this->connection->query($sql, [':grpid' => $id]);
    return $query->fetchAll();
  }

  /**
   * Calculates the expiration date of forms,if expired,displays 'Expired'.
   *
   * @param mixed $startDT
   *   Start date.
   * @param mixed $endDT
   *   End date.
   *
   * @return string
   *   Returns a string if form expired 'yes' otherwise 'no'.
   */
  public function checkFormsExpiry($startDT, $endDT) {
    date_default_timezone_set(date_default_timezone_get());
    $currDT = date('Y-m-d h:i:s a');
    $currDT = strtotime($currDT);
    $startDT = strtotime($startDT);
    $endDT = strtotime($endDT);
    if ($startDT > $currDT) {
      return 'no';
    }
    if ($endDT) {
      if (($currDT >= $startDT) && ($currDT <= $endDT)) {
        return 'no';
      }
      else {
        return 'yes';
      }
    }
    else {
      return 'no';
    }
  }

  /**
   * Calculates expiry date of a form.
   *
   * @param mixed $startDT
   *   The Start date.
   * @param mixed $endDT
   *   The End date.
   *
   * @return string
   *   Return yes if the form is expired otherwise returns no.
   */
  public function checkChannelFormsExpiry($startDT, $endDT) {
    date_default_timezone_set(date_default_timezone_get());
    $currDT = date('Y-m-d h:i:s a');
    $currDT = strtotime($currDT);
    $startDT = strtotime($startDT);
    $endDT = strtotime($endDT);
    if ($startDT > $currDT) {
      return 'no';
    }
    if ($endDT) {
      if (($currDT >= $startDT) && ($currDT <= $endDT)) {
        return 'no';
      }
      else {
        return 'yes';
      }
    }
    else {
      return 'no';
    }
  }

  /**
   * Get all the account details.
   *
   * @return array
   *   Returns all account details as an array to display.
   */
  public function getSettingsRecords() {
    $sql = "SELECT cnpstngs_ID,cnpstngs_frndlyname,cnpstngs_AccountNumber,cnpstngs_guid,cnpstngs_status,"
      . "cnpstngs_Date_Created,cnpstngs_Date_Modified FROM {dp_cnpcsettingsdtl}";
    $query = $this->connection->query($sql);
    return $query->fetchAll();
  }

}
